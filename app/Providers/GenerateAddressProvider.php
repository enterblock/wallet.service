<?php


namespace App\Providers;


use App\Service\coinGenerateAddress\BTCGenerateAddressService;
use App\Service\coinGenerateAddress\ETHGenerateAddressService;
use Illuminate\Support\ServiceProvider;

class GenerateAddressProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ETHGenerateAddressService', ETHGenerateAddressService::class);
        $this->app->bind('BTCGenerateAddressService', BTCGenerateAddressService::class);
        $this->app->bind('LTCGenerateAddressService', BTCGenerateAddressService::class);
        $this->app->bind('BCHGenerateAddressService', BTCGenerateAddressService::class);
        $this->app->bind('BSVGenerateAddressService', BTCGenerateAddressService::class);
    }

    public function provides()
    {
        return ['ETHGenerateAddressService', 'BTCGenerateAddressService', 'LTCGenerateAddressService', 'BCHGenerateAddressService'
            , 'BSVGenerateAddressService'];
    }
}