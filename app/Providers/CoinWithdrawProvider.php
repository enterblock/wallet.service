<?php


namespace App\Providers;


use App\Service\coinWithdraw\BTCWithdrawService;
use App\Service\coinWithdraw\ETHTokenWithdrawService;
use App\Service\coinWithdraw\ETHWithdrawService;
use App\Service\coinWithdraw\LTCWithdrawService;
use App\Service\coinWithdraw\XRPWithdrawService;
use Illuminate\Support\ServiceProvider;

class CoinWithdrawProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ETHWithdrawService', ETHWithdrawService::class);
        $this->app->bind('ETHTokenWithdrawService', ETHTokenWithdrawService::class);
        $this->app->bind('BTCWithdrawService', BTCWithdrawService::class);
        $this->app->bind('LTCWithdrawService', LTCWithdrawService::class);
        $this->app->bind('BCHWithdrawService', BTCWithdrawService::class);
        $this->app->bind('BSVWithdrawService', BTCWithdrawService::class);
        $this->app->bind('XRPWithdrawService', XRPWithdrawService::class);
    }

    public function provides()
    {
        return ['ETHWithdrawService', 'ETHTokenWithdrawService', 'BTCWithdrawService', 'LTCWithdrawService', 'BCHWithdrawService', 'BSVWithdrawService', 'XRPWithdrawService'];
    }
}