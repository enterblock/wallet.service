<?php

namespace App\Providers;

use App\Service\coinDaemonLog\BCHDaemonLogService;
use App\Service\coinDaemonLog\BSVDaemonLogService;
use App\Service\coinDaemonLog\BTCDaemonLogService;
use App\Service\coinDaemonLog\CoinDaemonLogAbstract;
use App\Service\coinDaemonLog\ETHDaemonLogService;
use App\Service\coinDaemonLog\ETHTokenDaemonLogService;
use App\Service\coinDaemonLog\LTCDaemonLogService;
use App\Service\coinDaemonLog\TRXDaemonLogService;
use App\Service\coinDaemonLog\USDTDaemonLogService;
use App\Service\coinDaemonLog\XRPDaemonLogService;
use Illuminate\Support\ServiceProvider;

class CoinDaemonLogProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ETHDaemonLogService', ETHDaemonLogService::class);
        $this->app->bind('ETHTokenDaemonLogService', ETHTokenDaemonLogService::class);
        $this->app->bind('XRPDaemonLogService', XRPDaemonLogService::class);
        $this->app->bind('BTCDaemonLogService', BTCDaemonLogService::class);
        $this->app->bind('BCHDaemonLogService', BCHDaemonLogService::class);
        $this->app->bind('BSVDaemonLogService', BSVDaemonLogService::class);
        $this->app->bind('LTCDaemonLogService', LTCDaemonLogService::class);
        $this->app->bind('TRXDaemonLogService', TRXDaemonLogService::class);
        $this->app->bind('USDTDaemonLogService', USDTDaemonLogService::class);
    }

    public function provides()
    {
        return ['ETHDaemonLogService', 'ETHTokenDaemonLogService', 'XRPTokenDaemonLogService',
            'BTCDaemonLogService', 'BCHDaemonLogService', 'BSVDaemonLogService', 'LTCDaemonLogService',
            'TRXDaemonLogService', 'USDTDaemonLogService'];
    }
}
