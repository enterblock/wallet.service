<?php

namespace App\Jobs;

use App\Exceptions\ApplicationException;
use Exception;
use App\Service\coinDaemonLog\CoinDaemonLogFactory;
use App\Service\CurrencyService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class DaemonLogReceiptJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $retryAfter = 60;
    private $daemonTxs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($daemonTxs)
    {
        $this->daemonTxs = $daemonTxs;
    }

    /**
     * @param CoinDaemonLogFactory $daemonLogFactory
     * @param CurrencyService $currencyService
     * @throws \App\Exceptions\ApplicationException
     * @throws \App\Exceptions\JobRetryException
     */
    public function handle(CoinDaemonLogFactory $daemonLogFactory, CurrencyService $currencyService)
    {
        foreach ($this->daemonTxs as $daemonTx) {
            try {
                $coin = $currencyService->getCurrency($daemonTx->currency_id);
                $daemonLogService = $daemonLogFactory->build($coin->currency_mark);
                $daemonLogService->checkProcess($coin, $daemonTx);
            } catch (ApplicationException $e) {
                if ($e->getCode() === ApplicationException::ABNORMAL_DATA) {
                    Log::error($e->getMessage());
                    continue;
                }
            }
        }
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::debug('failed run');
    }
}
