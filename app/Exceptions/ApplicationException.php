<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오전 11:28
 */

namespace App\Exceptions;


use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Throwable;

class ApplicationException extends \Exception implements Responsable
{
    // 요청 관련 10000~10099
    const REQUEST_PARAM = 10000;

    // 출금 관련 10200~10299
    const AMOUNT_SHORTAGE = 10200;
    const WITHDRAW_INTERVAL = 10201;
    const INACTIVE_WITHDRAW = 10202;

    // 코인 정보 관련 10300~10399
    const UNSUPPORTED_COIN = 10300;
    const EXCEPTION_TX_HASH = 10301;
    const NOT_GENERATE_ADDRESS = 10302;
    const CHECK_COIN_ENV = 10303;

    // 입금 관련 10400~10499
    const INACTIVE_DEPOSIT = 10400;


    // DB 관련 10500~10599
    const ABNORMAL_DATA = 10500;
    const UPDATE_FAIL = 10501;

    // 회원 정보 관련 10600~10699
    const NOT_MEMBER = 10600;
    const LOW_BALANCE = 10601;

    private $errMessages = [
        self::AMOUNT_SHORTAGE => '금액부족',
        self::UNSUPPORTED_COIN => '지원하지 않는 코인',
        self::EXCEPTION_TX_HASH => '예외처리된 Transaction',
        self::LOW_BALANCE => '잔액 부족',
        self::ABNORMAL_DATA => 'DB 데이터 정합성 확인',
        self::NOT_MEMBER => '회원정보가 존재하지 않음',
        self::REQUEST_PARAM => '요청 파라메터 에러',
        self::NOT_GENERATE_ADDRESS => '생성된 주소 없음',
        self::UPDATE_FAIL => 'DB 업데이트 실패',
        self::CHECK_COIN_ENV => '코인 환경변수 체크 필요',
        self::WITHDRAW_INTERVAL => '출금 interval 에러',
        self::INACTIVE_WITHDRAW => '출금 비활성 상태',
        self::INACTIVE_DEPOSIT => '입금 비활성 상태',
    ];

    public function __construct(int $code = 0, $errMessages=null, Throwable $previous = null)
    {

        if (!is_null($errMessages)) {
            parent::__construct($errMessages, $code, $previous);
        } else if(Arr::has($this->errMessages, $code)){
            parent::__construct(trans($this->errMessages[$code]), $code, $previous);
        } else {
            parent::__construct("", $code, $previous);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        $resp = response()->json([
            'status' => 'fail',
            'code' => $this->getCode(),
            'data' => trans($this->getMessage())
        ]);

        if($this->getCode() < 10100) {
            return $resp->setStatusCode(400);
        }
        return $resp->setStatusCode(500);
    }
}