<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 17
 * Time: 오전 10:30
 */

namespace App\Exceptions;


use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Throwable;

class DaemonException extends \Exception implements Responsable
{
    // 데몬 요청 관련
    const UNSURPORTED_REQUEST = 20000;
    const UNSURPORTED_SYMBOL = 20001;

    // 데몬 응답 관련 20100~20199
    const RECEIPT_EMPTY_RESPONSE = 20100;
    const CREATE_ADDR_ERR = 20101;
    const WITHDRAW_ERR = 20102;
    const RECEIPT_ERR = 20104;
    const SUPER_APPROVE_ERR = 20105;
    const GET_BLOCK_ERR = 20106;
    const GET_BLOCK_HEIGHT_ERR = 20107;
    const CREATE_COIN_ADDR_ERR = 20108;
    const COIN_SEND_ERR = 20109;
    const UNLOCK_ERR = 20110;
    const LOCK_ERR = 20111;
    const SIGN_ERR = 20112;
    const GET_LIST_BLOCK_ERR = 20113;
    const GET_BALANCE_ERR = 20114;
    const VALID_ADDRESS_ERR = 20115;

    # 설정 값 제한 20200~20299
    const GAS_PRICE_LIMIT_ERR = 20200;
    const GAS_LIMIT_ERR = 20201;

    private $errMessages = [
        self::RECEIPT_EMPTY_RESPONSE=>'Receipt 정보 없음',
        self::CREATE_ADDR_ERR=>'주소생성 에러',
        self::WITHDRAW_ERR=>'출금 에러',
        self::RECEIPT_ERR=>'recepit 에러',
        self::UNSURPORTED_REQUEST=>'지원하지 않는 요청',
        self::SUPER_APPROVE_ERR=>'super approve 에러',
        self::UNSURPORTED_SYMBOL=>'지원하지 않는 코인 심볼',
        self::GET_BLOCK_ERR=>'블록정보 조회 에러',
        self::GET_BLOCK_HEIGHT_ERR=>'최신 블록 조회 에러',
        self::CREATE_COIN_ADDR_ERR=>'코인 주소 생성 에러',
        self::COIN_SEND_ERR=>'코인 Send 에러',
        self::UNLOCK_ERR=>'Unlock 에러',
        self::LOCK_ERR=>'Lock 에러',
        self::GAS_PRICE_LIMIT_ERR=>'가스 가격 제한 에러',
        self::GAS_LIMIT_ERR=>'가스 제한 에러',
        self::SIGN_ERR=>'SIGN Err',
        self::GET_LIST_BLOCK_ERR=>'블록 트랜잭션 리스트 조회 에러',
        self::GET_BALANCE_ERR=>'잔액 조회 에러',
        self::VALID_ADDRESS_ERR=>'주소 검증 에러',
    ];

    public function __construct(int $code = 0, $errMessages=null, Throwable $previous = null)
    {
        if (!is_null($errMessages)) {
            parent::__construct($errMessages, $code, $previous);
        } else if(Arr::has($this->errMessages, $code)){
            parent::__construct(trans($this->errMessages[$code]), $code, $previous);
        } else {
            parent::__construct("", $code, $previous);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        $resp = response()->json([
            'status' => 'fail',
            'code' => $this->getCode(),
            'data' => $this->getMessage()
        ]);

        if($this->getCode() < 20100) {
            return $resp->setStatusCode(400);
        }
        return $resp->setStatusCode(500);
    }
}