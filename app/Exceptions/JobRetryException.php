<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오전 11:28
 */

namespace App\Exceptions;


use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Throwable;

class JobRetryException extends \Exception implements Responsable
{
    // 입출금 관련 1000~10099
    const CONFIRM_CHECK = 10000;

    private $errMessages = [
        self::CONFIRM_CHECK=>'금액부족',
    ];

    public function __construct(int $code = 0, Throwable $previous = null)
    {

        if(Arr::has($this->errMessages, $code)){
            parent::__construct(trans($this->errMessages[$code]), $code, $previous);
        } else {
            parent::__construct("", $code, $previous);
        }
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        $resp = response()->json([
            'status' => 'fail',
            'data' => $this->getMessage()
        ]);

        if($this->getCode() < 10100) {
            return $resp->setStatusCode(400);
        }
        return $resp->setStatusCode(500);
    }

    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        Log::debug('검증 재시도');
    }
}