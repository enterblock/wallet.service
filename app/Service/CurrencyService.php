<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 9. 26
 * Time: 오후 6:43
 */

namespace App\Service;


use App\Exceptions\ApplicationException;
use App\Models\Currency;
use App\Models\CurrencyInfo;
use App\Repositories\CurrencyCtRepository;
use App\Repositories\CurrencyInfoRepository;
use App\Repositories\CurrencyRepository;

class CurrencyService
{
    /**
     * @var CurrencyInfoRepository
     */
    private $currencyInfoRepository;
    /**
     * @var CurrencyCtRepository
     */
    private $currencyCtRepository;

    public function __construct(CurrencyInfoRepository $currencyInfoRepository, CurrencyCtRepository $currencyCtRepository)
    {
        $this->currencyInfoRepository = $currencyInfoRepository;
        $this->currencyCtRepository = $currencyCtRepository;
    }

    public function getCurrency($id)
    {
        return $this->currencyInfoRepository->find($id);
    }

    /**
     * @param $coinSymbol
     * @return CurrencyInfo mixed
     */
    public function findBySymbol($coinSymbol)
    {
        return $this->currencyInfoRepository->findBySymbol($coinSymbol);
    }

    /**
     * @param $coinSymbol
     * @return mixed|null
     * @throws ApplicationException
     */
    public function getCurrencyIdBySymbol($coinSymbol)
    {
        $currencyId = null;
        if (!is_null($coinSymbol)) {
            $currency = $this->findBySymbol($coinSymbol);
            if (is_null($currency)) {
                throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
            }
            $currencyId = $currency->id;
        }
        return $currencyId;
    }

    public function checkEnableWithdraw($currencyId)
    {
        return $this->currencyInfoRepository->where(['id'=>$currencyId, 'withdraw_status'=>1])->get()->isNotEmpty();
    }

    public function getSubAddressTypeCurrencyIdArr()
    {
        return $this->currencyInfoRepository->where(['address_type'=>CurrencyInfoRepository::ADDRESS_TYPE_SUB_ADDRESS])->pluck('id')->toArray();
    }

    public function getGenerateAddressCurrencyCt()
    {
        return $this->currencyCtRepository->where(['generate_address_active'=>CurrencyCtRepository::GENERATE_ADDRESS_ACTIVE])->get();
    }
}