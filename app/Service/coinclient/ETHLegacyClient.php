<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 6:59
 */

namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Repositories\CurrencyTokenRepository;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;
use App\Utils\StringUtils;
use Illuminate\Support\Facades\Log;
use kornrunner\Keccak;

class ETHLegacyClient implements CoinClientInterface
{
    private $nodeIp;
    private $nodePort;
    private $uri;

    const ERC20_TRANSFER_FUNC = "transfer(address,uint256)";
    const ERC20_BALANCE_OF_FUNC = "balanceOf(address)";

    /**
     * ETHClient constructor.
     */
    public function __construct()
    {
        $this->nodeIp = env("ETH_NODE_IP");
        $this->nodePort = env("ETH_NODE_PORT");
        $this->uri=$this->nodePort?$this->nodeIp . ":" . $this->nodePort:$this->nodeIp;
    }

    /**
     * @param $txid
     * @return mixed
     * @throws DaemonException
     */
    public function getTransactionReceipt($txid)
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('eth_getTransactionReceipt', rand(1, 9999), [$txid]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::RECEIPT_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @param $txid
     * @return mixed
     * @throws DaemonException
     */
    public function getTransaction($txid)
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('eth_getTransactionByHash', rand(1, 9999), [$txid]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::RECEIPT_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }


    /**
     * @param $blockNumber
     * @param null $endBlockNum
     * @return mixed
     * @throws DaemonException
     */
    public function getBlock($blockNumber, $endBlockNum=null)
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $blockNumberHex = "0x".dechex($blockNumber);
        $req = new JsonRpcRequest('eth_getBlockByNumber', rand(1, 9999), [$blockNumberHex, true]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @return mixed
     * @throws DaemonException
     */
    public function getBlockHeight()
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('eth_blockNumber', rand(1, 9999), []);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_HEIGHT_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @return mixed
     * @throws DaemonException
     */
    public function createCoinAddr()
    {
        $userAddrPass = env("ETH_OUT_ADDRESS_PASSWORD");
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('personal_newAccount', rand(1, 9999), [$userAddrPass]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::CREATE_COIN_ADDR_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @param $currency
     * @param $addr
     * @return mixed
     * @throws DaemonException
     */
    public function balance($currency, $addr)
    {
        if (is_null($currency->parent)) {
            $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
            $req = new JsonRpcRequest('eth_getBalance', rand(1, 9999), [$addr,'latest']);

            $res = $cli->appendRequest($req)->send();
            if(isset($res["error"]) && !is_null($res["error"])) {
                throw new DaemonException(DaemonException::COIN_SEND_ERR, print_r($res["error"], true));
            }
            return $res["result"];
        } else {
            // ERC20
            $balanceOfData = $this->generateBalanceData($addr);
            $currencyTokenInfo = $currency->token->first();

            $sendAttributes = [
                [
                    "to"=>$currencyTokenInfo->token_id,
                    "data"=>$balanceOfData
                ],
                'latest'
            ];

            return $this->ethCall($sendAttributes);
        }
    }

    /**
     * @param $currency
     * @param $attributes
     * @return mixed
     * @throws DaemonException
     */
    public function merge($currency, $attributes)
    {
        $password = env('ETH_OUT_ADDRESS_PASSWORD');
        if (is_null($currency->parent)) {
            $pow = bcpow('10', $currency->decimal_point);
            $hexAmount = StringUtils::bcDechex(bcmul($attributes['amount'], $pow));
            $sendAttributes = [
                [
                    "from"=>$attributes['sender'],
                    "to"=>env('ETH_OUT_ADDRESS'),
                    "value"=>"0x" . $hexAmount,
                ],
                $password
            ];
        } else {
            // ERC20
            $pow = bcpow('10', $currency->decimal_point);
            $attributes['hexAmount'] = StringUtils::bcDechex(bcmul($attributes['amount'], $pow));
            $attributes['receiver'] = env('ETH_OUT_ADDRESS');
            $sendData = $this->generateSendData($attributes);
            $currencyTokenInfo = $currency->token->first();

            $sendAttributes = [
                [
                    "from"=>$attributes['sender'],
                    "to"=>$currencyTokenInfo->token_id,
                    "value"=>"0x0",
                    "data"=>$sendData
                ],
                $password
            ];
        }
        return $this->sendTrnsaction($sendAttributes);
    }

    /**
     * @param $currency
     * @param $attributes
     * @return mixed
     * @throws DaemonException
     */
    public function send($currency, $attributes)
    {
        $password = env('ETH_OUT_ADDRESS_PASSWORD');
        if (is_null($currency->parent)) {
            $pow = bcpow('10', $currency->decimal_point);
            $hexAmount = StringUtils::bcDechex(bcmul($attributes['amount'], $pow));
            $sendParams = [
                "from"=>env('ETH_OUT_ADDRESS'),
                "to"=>$attributes['receiver'],
                "value"=>"0x" . $hexAmount,
            ];
        } else {
            // ERC20
            $pow = bcpow('10', $currency->decimal_point);
            $attributes['hexAmount'] = StringUtils::bcDechex(bcmul($attributes['amount'], $pow));
            $sendData = $this->generateSendData($attributes);
            $currencyTokenInfo = $currency->token->first();
            $sendParams = [
                "from"=>env('ETH_OUT_ADDRESS'),
                "to"=>$currencyTokenInfo->token_id,
                "value"=>"0x0",
                "data"=>$sendData
            ];
        }

        /** FEE 조정로직 */
        list($setGasPrice, $setGas) = $this->getFeeInfo($sendParams);

        $sendParams['gasPrice'] = '0x'.StringUtils::bcDechex($setGasPrice);
        $sendParams['gas'] = '0x'.StringUtils::bcDechex($setGas);

        Log::debug("ETH/ERC20 send max fee info: ".bcdiv(bcmul($setGasPrice, $setGas), bcpow('10','18'), '18'));
        $sendAttributes = [
            $sendParams,
            $password
        ];
        return $this->sendTrnsaction($sendAttributes);
    }

    /**
     * @param $attributes
     * @return mixed
     * @throws DaemonException
     */
    public function sendTrnsaction($attributes)
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('personal_sendTransaction', rand(1, 9999), $attributes);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @param $attributes
     * @return mixed
     * @throws DaemonException
     */
    private function ethCall($attributes)
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('eth_call', rand(1, 9999), $attributes);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @param $addr
     * @param $password
     * @param int $secends
     * @return mixed
     * @throws DaemonException
     */
    public function unlock($addr, $password, $secends=10)
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('personal_unlockAccount', rand(1, 9999), [$addr, $password, $secends]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::UNLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @throws DaemonException
     */
    public function lock()
    {
        throw new DaemonException(DaemonException::UNSURPORTED_REQUEST);
    }

    /**
     * @return mixed
     * @throws DaemonException
     */
    public function getGasPrice()
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('eth_gasPrice', rand(1, 9999), []);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::UNLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @param $attributes
     * @return mixed
     * @throws DaemonException
     */
    public function getEstimateGas($attributes)
    {
        $cli = new JsonRpcClient($this->uri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('eth_estimateGas', rand(1, 9999), [$attributes]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::UNLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @param $attributes
     * @return string
     * @throws \Exception
     */
    public function generateSendData($attributes)
    {
        $funcHex = Keccak::hash(self::ERC20_TRANSFER_FUNC, '256');
        $funcHex = substr($funcHex, 0, 8);
        $toAddrData = str_pad(preg_replace('/^0x/','',$attributes['receiver']),64,'0', STR_PAD_LEFT);
        $amountData = str_pad($attributes['hexAmount'],64,'0', STR_PAD_LEFT);

        return "0x" . $funcHex . $toAddrData . $amountData;
    }

    /**
     * @param $addr
     * @return string
     * @throws \Exception
     */
    private function generateBalanceData($addr)
    {
        $funcHex = Keccak::hash(self::ERC20_BALANCE_OF_FUNC, '256');
        $funcHex = substr($funcHex, 0, 8);
        $addrData = str_pad(preg_replace('/^0x/','',$addr),64,'0', STR_PAD_LEFT);

        return "0x" . $funcHex . $addrData;
    }

    /**
     * FEE 조정로직
     *
     * @param array $sendParams
     * @return array
     * @throws DaemonException
     */
    private function getFeeInfo(array $sendParams): array
    {
        $gasPriceHex = $this->getGasPrice();
        $gasPriceDec = StringUtils::bcHexdec($gasPriceHex);
        $rateGasPrice = bcmul($gasPriceDec, env('GAS_PRICE_RATE'));
        $setGasPrice = $rateGasPrice;
        if (bccomp($rateGasPrice, env('GAS_PRICE_LIMIT')) >= 0) {
            if (bccomp($gasPriceDec, env('GAS_PRICE_LIMIT')) >= 0) {
                throw new DaemonException(DaemonException::GAS_PRICE_LIMIT_ERR);
            }
            $setGasPrice = $gasPriceDec;
        }

        $estimateGasHex = $this->getEstimateGas($sendParams);
        $estimateGasDec = StringUtils::bcHexdec($estimateGasHex);
        $rateEstimateGas = bcmul($estimateGasDec, env('GAS_RATE'));
        $setGas = $rateEstimateGas;
        if (bccomp($setGas, env('GAS_LIMIT')) >= 0) {
            if (bccomp($estimateGasDec, env('GAS_PRICE_LIMIT')) >= 0) {
                throw new DaemonException(DaemonException::GAS_LIMIT_ERR);
            }
            $setGas = $estimateGasDec;
        }
        return array($setGasPrice, $setGas);
    }
}