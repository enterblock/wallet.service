<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 7:10
 */

namespace App\Service\coinclient;


interface CoinClientInterface
{
    public function createCoinAddr();
    public function send($currency, $attributes);
    public function getTransactionReceipt($txid);
    public function getBlock($blockNumber, $endBlockNumber);
    public function getBlockHeight();
    public function unlock($addr, $password, $secends);
    public function lock();
}