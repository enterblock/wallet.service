<?php


namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Repositories\DaemonTxRepository;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;
use App\Utils\StringUtils;

class USDTClient extends ETHClient
{
    protected $symbol = "USDT";
    /**
     * @param $currency
     * @param $attributes
     * @return mixed
     * @throws DaemonException
     */
    public function merge($currency, $attributes)
    {
        $signRaw = $this->erc20MergeSign($currency, $attributes);
        return $this->sendRawTrnsaction($signRaw);
    }

    protected function erc20MergeSign($currency, $attributes)
    {
        $currencyTokenInfo = $currency->token->first();
        $pow = bcpow('10', $currency->decimal_point);
        $cli = new JsonRpcClient($this->signUri, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('eth_signTransaction', rand(1, 9999), [
            "from"=>$attributes['sender'],
            "to"=>env('USDT_OUT_ADDRESS'),
            "contract"=>$currencyTokenInfo->token_id,
            "fee_addr"=>env('USDT_OUT_ADDRESS'),
            "manager"=>env('USDT_MANAGER_ADDRESS'),
            "amount"=>bcmul($attributes['amount'], $pow),
            "pass"=>env('USDT_OUT_ADDRESS_PASSWORD'),
            "nonce"=> "no_pending",
        ]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::SIGN_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    /**
     * @param $currency
     * @param $addr
     * @return mixed
     * @throws DaemonException
     */
    public function balance($currency, $addr)
    {
        // ERC20
        $balanceOfData = $this->generateBalanceData($addr);
        $currencyTokenInfo = $currency->token->first();

        $sendAttributes = [
            [
                "to"=>$currencyTokenInfo->token_id,
                "data"=>$balanceOfData
            ],
            'latest'
        ];

        $balanceHex = $this->ethCall($sendAttributes);
        $balance = StringUtils::bcHexdec($balanceHex);
        $balance = bcdiv($balance, bcpow('10', $currency->decimal_point), $currency->decimal_point);
        return $balance;
    }
}