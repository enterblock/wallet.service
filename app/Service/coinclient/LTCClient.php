<?php


namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;

class LTCClient extends BTCClient
{
    protected $nodeIp;
    protected $nodePort;
    protected $nodeUser;
    protected $nodePassword;
    protected $uri;

    /**
     * BCHClient constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->nodeIp = env("LTC_NODE_IP");
        $this->nodePort = env("LTC_NODE_PORT");
        $this->nodeUser = env("LTC_NODE_USER");
        $this->nodePassword = env("LTC_NODE_PASSWORD");
        $this->walletPass = env("LTC_OUT_PASSWORD");
        $this->walletUnlockTime = env("LTC_UNLOCK_TIME");
        $this->uri=$this->nodePort?$this->nodeIp . ":" . $this->nodePort:$this->nodeIp;
    }
}