<?php


namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Repositories\DaemonTxRepository;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;

class USDTBTCClient extends BTCClient
{
    protected $nodeIp;
    protected $nodePort;
    protected $nodeUser;
    protected $nodePassword;
    protected $uri;

    const MAX_LIST_TRANSACTIONS_COUNT = 100000;
    const MAX_LIST_TRANSACTIONS_END_BLOCK = 999999999;
    const PROPERTY_ID = 31;

    /**
     * BCHClient constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->nodeIp = env("USDT_NODE_IP");
        $this->nodePort = env("USDT_NODE_PORT");
        $this->nodeUser = env("USDT_NODE_USER");
        $this->nodePassword = env("USDT_NODE_PASSWORD");
        $this->walletPass = env("USDT_OUT_PASSWORD");
        $this->walletUnlockTime = env("USDT_UNLOCK_TIME");
        $this->uri=$this->nodePort?$this->nodeIp . ":" . $this->nodePort:$this->nodeIp;
    }

    public function getListBlock($blockNumber, $endBlockNum = null)
    {
        $endBlockNum = $endBlockNum?:self::MAX_LIST_TRANSACTIONS_END_BLOCK;
        return $this->omniListTransactions(self::MAX_LIST_TRANSACTIONS_COUNT, 0, $blockNumber, $endBlockNum);
    }

    public function omniListTransactions($count, $skip, $start, $end)
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $params = [
            '*',
            intval($count),
            intval($skip),
            intval($start),
            intval($end),
        ];
        $req = new JsonRpcRequest('omni_listtransactions', rand(1, 9999), $params);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_LIST_BLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function send($currency, $attributes)
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $params = [
            $attributes['sender'],
            $attributes['receiver'],
            self::PROPERTY_ID,
            $attributes['amount'],
            $attributes['fee_addr'],
        ];
        $req = new JsonRpcRequest('omni_funded_send', rand(1, 9999), $params);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function merge($currency, $attributes)
    {
        if (!empty($this->walletPass)) {
            $this->unlock('', $this->walletPass, intval($this->walletUnlockTime));
        }
        $attributes['receiver'] = env("USDT_OUT_ADDRESS");
        $attributes['fee_addr'] = env("USDT_OUT_ADDRESS");
        return;
        return $this->send($currency, $attributes);
    }

    public function balance($currency, $addr)
    {
        $usdtBalance = "0";
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $params = [
            $addr,
        ];
        $req = new JsonRpcRequest('omni_getallbalancesforaddress', rand(1, 9999), $params);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BALANCE_ERR, print_r($res["error"], true));
        }

        $balanceArr = $res["result"];
        foreach ($balanceArr as $balanceInfo) {
            if ($balanceInfo['propertyid'] == self::PROPERTY_ID) {
                $usdtBalance = bcadd($usdtBalance, $balanceInfo["balance"], $currency->decimal_point);
            }
        }

        return $usdtBalance;
    }

    public function getBTCListBlock($blockHash)
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        if (is_null($blockHash)) {
            $params = [];
        } else {
            $params = [$blockHash];
        }
        $req = new JsonRpcRequest('listsinceblock', rand(1, 9999), $params);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_LIST_BLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }
}