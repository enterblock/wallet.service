<?php


namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;
use GuzzleHttp\Client;

class XRPClient implements CoinClientInterface
{
    private $url;
    private $withdrawUrl;
    private $withdrawIP;
    private $withdrawPort;
    const XRP_STATUS_SUCCESS = 'success';
    public function __construct()
    {
        $this->withdrawIP = env('XRP_WITHDRAW_RPC_IP');
        $this->withdrawPort = env('XRP_WITHDRAW_RPC_PORT');
        $this->url = env("XRP_BASE_RPC_URL");
        $this->withdrawUrl = $this->withdrawPort?$this->withdrawIP . ":" . $this->withdrawPort:$this->withdrawIP;
    }

    public function createCoinAddr()
    {
        throw new DaemonException(DaemonException::UNSURPORTED_REQUEST);
    }

    public function send($currency, $attributes)
    {
        $pow = bcpow('10', $currency->decimal_point);
        $amount = bcmul($attributes['amount'], $pow);
        $cli = new JsonRpcClient($this->withdrawUrl, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('xrp_sendTranaction', rand(1, 9999), [
            "from" => env("XRP_ADDRESS"),
            "to" => $attributes['receiver'],
            "memo" => $attributes['receiver_sub'],
            "password" => env("XRP_PASSWORD"),
            "amount" => $amount,
        ]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR);
        }
        return $res["result"];
    }

    public function sign($attributes, $secret)
    {
        $secret = 'ss7mCcLfhGgT9nrwCyzLk1b3EaNNF';
        $cli = new JsonRpcClient($this->url, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('sign', rand(1, 9999), [
            [
                "offline" => false,
                "secret" => $secret,
                "tx_json" => [
                    "Account" => "rar3BiJAfkJFFB6vYMGydN6F1CEgWMJMdW",
                    "Amount" => [
                        "currency" => "XRP",
                        "issuer" => "rar3BiJAfkJFFB6vYMGydN6F1CEgWMJMdW",
                        "value" => "1"
                    ],
                    "Destination" => "rMvAtPTQNK6YZDzbHgH5iBxy2cjGDdrWi5",
                    "TransactionType" => "Payment"
                ]
            ]
        ]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR);
        }
        return $res["result"];
    }

    public function getTransactionReceipt($txid)
    {
        $cli = new JsonRpcClient($this->url, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('tx', rand(1, 9999), [
            [
                "transaction" => $txid,
                "binary" => false
            ]
        ]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"]) && $res["result"]['STATUS']!=self::XRP_STATUS_SUCCESS) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR);
        }
        return $res["result"];
    }

    public function getBlock($block=-1,$endBlock=-1)
    {
        $cli = new JsonRpcClient($this->url, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('account_tx', rand(1, 9999), [
            [
                "account" => env('XRP_ADDRESS'),
                "ledger_index_min" => $block,
                "ledger_index_max" => $endBlock,
                "binary" => false,
                "forward" => true
            ]
        ]);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"]) && $res["result"]['STATUS']!=self::XRP_STATUS_SUCCESS) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR);
        }
        return $res["result"];
    }

    public function getBlockHeight()
    {
        $cli = new JsonRpcClient($this->url, ['headers' => ['Content-Type'=>'application/json']]);
        $req = new JsonRpcRequest('ledger_current', rand(1, 9999), []);

        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR);
        }
        return $res["result"];
    }

    public function unlock($addr, $password, $secends)
    {
        throw new DaemonException(DaemonException::UNSURPORTED_REQUEST);
    }

    public function lock()
    {
        throw new DaemonException(DaemonException::UNSURPORTED_REQUEST);
    }
}