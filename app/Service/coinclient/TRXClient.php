<?php

namespace App\Service\coinclient;

use App\Exceptions\DaemonException;
use App\Utils\StringUtils;
use IEXBase\TronAPI\Provider\HttpProvider;
use IEXBase\TronAPI\Tron;
use GuzzleHttp\Client;

class TRXClient implements CoinClientInterface
{
    private $nodeIp;

    private $fullNodePort;
    private $solNodePort;
    private $signPort;

    private $full_uri;
    private $sol_uri;
    private $sign_uri;

    private $client;

    /**
     * TRXClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client();

        $this->nodeIp = env("TRX_NODE_IP");
        $this->fullNodePort = env("TRX_FULL_NODE_PORT");
        $this->solNodePort= env("TRX_SOL_NODE_PORT");
        $this->signPort = env("TRX_SIGN_NODE_PORT");

        $this->full_uri = "http://". $this->nodeIp.":".$this->fullNodePort;
        $this->sol_uri = "http://". $this->nodeIp.":".$this->solNodePort;
        $this->sign_uri = "http://". $this->nodeIp.":".$this->signPort;
    }

    public function createCoinAddr()
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "trx_getNewAddress",
                "params" => [
                    "password" => env('TRX_OUT_ADDRESS_PASSWORD')
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody()); //case 1

        if(isset($resObj->error) && !is_null($resObj->error)) {
            throw new DaemonException(DaemonException::CREATE_COIN_ADDR_ERR, print_r($resObj->error, true));
        }
        return $resObj;
    }

    public function signTransaction($to, $amount)
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "trx_sendRawTx",
                "params" => [
                    "from" => env('TRX_OUT_ADDRESS'), // from address
                    "to" => $to, // to address
                    "amount" => $amount, // amount TRX SUN(1 SUN = 1/1000000 TRX)
                    "password" => env('TRX_OUT_ADDRESS_PASSWORD') // password
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());

        if(isset($resObj->error) && !is_null($resObj->error) && !isset($resObj->result->txID)) {
            throw new DaemonException(DaemonException::SIGN_ERR, print_r($resObj->error, true));
        }

        return $resObj;
    }

    public function signTRCTransaction($tokenId, $to, $amount)
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "trc10_sendRawTx",
                "params" => [
                    "from" => env('TRX_OUT_ADDRESS'), // from address
                    "to" => $to, // to address
                    "amount" => $amount, // amount BTT SUN (1 SUN = 1/1000000 BTT)
                    "password" => env('TRX_OUT_ADDRESS_PASSWORD'), // password
                    "id" => $tokenId // BTT tokenid
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());

        if(isset($resObj->error) && !is_null($resObj->error) && !isset($resObj->result->txID)) {
            throw new DaemonException(DaemonException::SIGN_ERR, print_r($resObj->error, true));
        }

        return $resObj;
    }

    public function signUSDTTransaction()
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "trc20_sendRawTx",
                "params" => [
                    "cont_usdt" => "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t", //usdt foundation address
                    "from" => "TRoX7W6yny5TjxivAPDC7MwLu2W1KtNogo", //from address
                    "to" => "TANbWMDqczpUb6gQovthTy4rHSiYegpKP1", //to address
                    "selector" => "transfer(address,uint256)", // legacy selector call
                    "amount" => "1", //amount USDT SUN (1 SUN = 1/1000000 USDT)
                    "password" => "123", // password
                    "feelimit" => "1000000" // fee for transfer max ( 1TRX = 1000000)
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());

        if(isset($resObj->error) && !is_null($resObj->error) && !isset($resObj->result->txID)) {
            throw new DaemonException(DaemonException::SIGN_ERR, print_r($resObj->error, true));
        }

        return $resObj;
    }

    public function signWINKTransaction($contract)
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "wink_sendRawTx",
                "params" => [
                    "con" => "TLa2f6VPqDgRE67v1736s7bJ8Ray5wYjU7", //wink foundation address
                    "from" => "TRoX7W6yny5TjxivAPDC7MwLu2W1KtNogo", //from address
                    "to" => "TANbWMDqczpUb6gQovthTy4rHSiYegpKP1", //to address
                    "selector" => "transfer(address,uint256)", // legacy selector call
                    "amount" => "1", //amount WINK SUN (1 SUN = 1/1000000 WINK)
                    "password" => "123", // password
                    "feelimit" => "1000000" // fee for transfer max ( 1TRX = 1000000)
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());

        if(isset($resObj->error) && !is_null($resObj->error) && !isset($resObj->result->txID)) {
            throw new DaemonException(DaemonException::SIGN_ERR, print_r($resObj->error, true));
        }

        return $resObj;
    }

    public function send($currency, $attributes)
    {

        $reps = $this->client->post($this->full_uri.'/wallet/broadcasttransaction', [
            'json' => $attributes->result
        ]);

        $repObj = json_decode($reps->getBody());

        if((isset($repObj->error) && !is_null($repObj->error)) || $repObj->result != true) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR, print_r($repObj->error, true));
        }

        return $attributes->result->txID;
    }

    public function getBlock($blockNumber, $endBlockNumber=null)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        $res = $tron->getBlock($blockNumber);
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR, print_r($res["error"], true));
        }

        return $res;
    }

    public function getBlockHeight()
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        $res = $tron->getCurrentBlock();
        if (isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_HEIGHT_ERR, print_r($res["error"], true));
        }
        return $res['block_header']['raw_data']['number'];

    }

    public function getBlockByHash($hash)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        //$hash="0000000000dbb3a036094bcc0f07201676b51dfb29d62726c7bcfc87c73a78a5";
        $res = $tron->getBlockByHash($hash);
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR, print_r($res["error"], true));
        }
        return $res;
    }

    public function validateAddress($address)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        //$address="TT67rPNwgmpeimvHUMVzFfKsjL9GZ1wGw8"; //hex address or string address both ok

        $res = $tron->validateAddress($address);
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::VALID_ADDRESS_ERR, print_r($res["error"], true));
        }
        return $res;
    }

    // 해당 txid의 block 정보 조회
    public function getTransactionInfo($txid)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        // $txid ="52980994ec35a56c01c208bd80daece64c30c32c874846fe43191e8dd89c8d26";//"f4319ccf5876d4af5a8ffe04871b0e61b95370837dae6e508393aae2e1b13e0a";

        $res = $tron->getTransactionInfo($txid);
        if(isset($res["error"]) && !is_null($res["error"])) {
            dd('error');
        }
        return $res;
    }

    public function getTransaction($txid)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        // $txid ="6f564731bf73cccbf4ee4b437832eae7f5edd45c350c79adcdc7ccbe0c876381";

        $res = $tron->getTransaction($txid);
        if(isset($res["error"]) && !is_null($res["error"])) {
            dd('error');
        }
        return $res;
    }


    public function converter($mode, $param)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');
        $res ='';

        if ($mode == 'toHex') {
            $res = $tron->toHex($param);
        } else if ($mode == 'fromHex') {
            $res = $tron->fromHex($param);
        }

        return $res;
    }

    public function getContractBalance($contract_address, $function_selector, $parameter, $owner_address)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        $reps = $this->client->post($this->full_uri . '/wallet/triggersmartcontract', [
            'json' => [
                "contract_address" => $contract_address,
                "function_selector" => $function_selector,
                "parameter" => $parameter,
                "owner_address" => $owner_address
            ]
        ]);

        $repObj = json_decode($reps->getBody());

        if (isset($repObj->result->code) && !is_null($repObj->result->code)) {
            throw new DaemonException(DaemonException::GET_BALANCE_ERR);
        }

        //
        $repObj->constant_result = ltrim($repObj->constant_result[0], "0");
        $repObj->constant_result = StringUtils::bcHexdec($repObj->constant_result);
        //

        return $repObj->constant_result;
    }

    // 트론 fee_address 전송.
    public function signConTransaction()
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "trx_contractRawTx",
                "params" => [
                    "cont_manager" => "TYfWVYkmiVw5GesV12Atm8AV9WDfPNqzis", //exchange contract manager address
                    "cont_user" => "TANbWMDqczpUb6gQovthTy4rHSiYegpKP1", // from contract user address
                    "to" => "TUA4q9D5ZgYUWP3oiesHUZ7eSH7UXk7MUq", // to address
                    "fee_address" => "TRoX7W6yny5TjxivAPDC7MwLu2W1KtNogo", //fee address (legacy type)
                    "amount" => "1", // amount TRX SUN (1 SUN = 1/1000000 TRX)
                    "id" => "0", // id : 0(TRX), id : 1002000(BTT)
                    "selector" => "sendTRX(address,address,uint256)", // contract selector for sending TRX
                    "password" => "123", // password
                    "feelimit" => "1000000" // maxlimit fee 1TRX = 1000000
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());
        if(isset($resObj->error) && !is_null($resObj->error)) {
            dd($resObj->error->message);
        } else if (!isset($resObj->result->txID))
        {
            dd('signtx error');
        }
        return $resObj;
    }

    public function signConBTTTransaction()
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "trc10_contractRawTx",
                "params" => [
                    "cont_manager" => "TYfWVYkmiVw5GesV12Atm8AV9WDfPNqzis", //exchange contract manager address
                    "cont_user" => "TANbWMDqczpUb6gQovthTy4rHSiYegpKP1", // from contract user address
                    "to" => "TUA4q9D5ZgYUWP3oiesHUZ7eSH7UXk7MUq", // to address
                    "fee_address" => "TRoX7W6yny5TjxivAPDC7MwLu2W1KtNogo", //fee address (legacy type)
                    "amount" => "1", // amount BTT SUN (1 SUN = 1/1000000 BTT)
                    "id" => "1002000", // id : 0(TRX), id : 1002000(BTT)
                    "password" => "123", // password
                    "feelimit" => "1000000" // maxlimit fee 1TRX = 1000000
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());
        if(isset($resObj->error) && !is_null($resObj->error)) {
            dd($resObj->error->message);
        } else if (!isset($resObj->result->txID))
        {
            dd('signtx error');
        }
        return $resObj;
    }

    public function signConUSDTTransaction()
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "contractRawTx",
                "params" => [
                    "cont_manager" => "TPRR3qSHbRrqkJ9XxcWXRBtrURJVFcWiEP", //exchange contract manager address
                    "id" => "TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t", //USDT foundation contract address
                    "cont_user" => "TQ3ZatpWccmpb4rHVdCi1PrMQHbABpJpgt", // from contract user address
                    "to" => "TRoX7W6yny5TjxivAPDC7MwLu2W1KtNogo", // to address
                    "fee_address" => "TRoX7W6yny5TjxivAPDC7MwLu2W1KtNogo", // //fee address (legacy type)
                    "amount" => "1", // amount USDT SUN (1 SUN = 1/1000000 USDT)
                    "password" => "123", // password
                    "feelimit" => "1000000" // maxlimit fee 1TRX = 1000000
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());
        if(isset($resObj->error) && !is_null($resObj->error)) {
            dd($resObj->error->message);
        } else if (!isset($resObj->result->txID))
        {
            dd('signtx error');
        }
        return $resObj;
    }

    public function signConWINKTransaction()
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "wink_contractRawTx",
                "params" => [
                    "cont_manager" => "TPRR3qSHbRrqkJ9XxcWXRBtrURJVFcWiEP", //exchange contract manager address
                    "cont_wink" => "TLa2f6VPqDgRE67v1736s7bJ8Ray5wYjU7", //WINK foundation contract address
                    "cont_user" => "TQ3ZatpWccmpb4rHVdCi1PrMQHbABpJpgt", // from contract user address
                    "to" => "TRoX7W6yny5TjxivAPDC7MwLu2W1KtNogo", // to address
                    "fee_address" => "TUA4q9D5ZgYUWP3oiesHUZ7eSH7UXk7MUq", // //fee address (legacy type)
                    "amount" => "1", // amount WINK SUN (1 SUN = 1/1000000 WINK)
                    "selector" => "sendTRC20(address,address,address,uint256)", // contract selector for sending WINK
                    "password" => "123", // password
                    "feelimit" => "1000000" // maxlimit fee 1TRX = 1000000
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());
        if(isset($resObj->error) && !is_null($resObj->error)) {
            dd($resObj->error->message);
        } else if (!isset($resObj->result->txID))
        {
            dd('signtx error');
        }
        return $resObj;
    }

    // 컨트랙트 주소 생성 인지?
    public function createContractAddr($contract_address, $fee_address, $usr_cnt, $password, $feelimit)
    {
        $reps = $this->client->post($this->sign_uri, [
            'json' => [
                "method" => "trx_createUsersTx",
                "params" => [
                    "contract" => $contract_address, //Manger contract address
                    "fee_address" => $fee_address, //fee address (legacy type)
                    "usr_cnt" => $usr_cnt, //to address
                    "selector" => "newUser(uint256)", // legacy selector call
                    "password" => $password, // password
                    "feelimit" => $feelimit // fee for transfer max ( 1TRX = 1000000).
                ],
                "id" => 1,
                "jsonrpc" => "2.0"
            ]
        ]);

        $resObj = json_decode($reps->getBody());
        if(isset($resObj->error) && !is_null($resObj->error)) {
            dd($resObj->error->message);
        }
        return $resObj;
    }

    // 주소 조회
    public function getContractUsers($contract_address, $parameter, $owner_address)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        $reps = $this->client->post($this->full_uri.'/wallet/triggersmartcontract', [
            'json' => [
                "contract_address" => $contract_address,
                "function_selector" => 'Users(uint256)',
                "parameter" => $parameter,
                "owner_address" => $owner_address
            ]
        ]);

        $repObj = json_decode($reps->getBody());

        if(isset($repObj->result->code) && !is_null($repObj->result->code))
        {
            dd(hex2bin($repObj->result->message));
        }

        //contract return useraddress -> 0 pad remove -> add '41' -> (hex->string)conversion
        $repObj->constant_result = ltrim($repObj->constant_result[0], "0");
        $repObj->constant_result = '41'.$repObj->constant_result;
        $repObj->constant_result = $tron->fromHex($repObj->constant_result);
        //

        return $repObj->constant_result;
    }


    // 컨트랙트 주소 개수
    public function getUsersLength($contract_address, $owner_address)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        $reps = $this->client->post($this->full_uri . '/wallet/triggersmartcontract', [
            'json' => [
                "contract_address" => $contract_address,
                "function_selector" => 'getUsersLength()',
                "parameter" => '',
                "owner_address" => $owner_address
            ]
        ]);

        $repObj = json_decode($reps->getBody());

        if (isset($repObj->result->code) && !is_null($repObj->result->code)) {
            dd(hex2bin($repObj->result->message));
        }

        //contract return useraddress -> 0 pad remove -> add '41' -> (hex->string)conversion
        $repObj->constant_result = ltrim($repObj->constant_result[0], "0");
        $repObj->constant_result = hexdec($repObj->constant_result);
        //

        return $repObj->constant_result;
    }

    // 주소 조회
    public function getUsers($contract_address, $owner_address, $parameter)
    {
        $tron = new Tron(new HttpProvider($this->full_uri),
            new HttpProvider($this->sol_uri),
            new HttpProvider($this->sol_uri));

        $tron->setDefaultBlock('latest');

        $reps = $this->client->post($this->full_uri.'/wallet/triggersmartcontract', [
            'json' => [
                "contract_address" => $contract_address,
                "function_selector" => 'getusers(uint256,uint256)',
                "parameter" => $parameter,
                "owner_address" => $owner_address
            ]
        ]);

        $repObj = json_decode($reps->getBody());

        if(isset($repObj->result->code) && !is_null($repObj->result->code))
        {
            dd(hex2bin($repObj->result->message));
        }

        //dd($tron->fromHex('41046c5a75854c3fd1a6663f5fd2069b74e8d787a0'));

        return $repObj->constant_result;
    }

    public function getTransactionReceipt($txid)
    {
        // TODO: Implement getTransactionReceipt() method.
    }

    public function unlock($addr, $password, $secends)
    {
        // TODO: Implement unlock() method.
    }

    public function lock()
    {
        // TODO: Implement lock() method.
    }
}