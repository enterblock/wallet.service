<?php


namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;

class BSVClient extends BTCClient
{
    protected $nodeIp;
    protected $nodePort;
    protected $nodeUser;
    protected $nodePassword;
    protected $uri;

    /**
     * BCHClient constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->nodeIp = env("BSV_NODE_IP");
        $this->nodePort = env("BSV_NODE_PORT");
        $this->nodeUser = env("BSV_NODE_USER");
        $this->nodePassword = env("BSV_NODE_PASSWORD");
        $this->walletPass = env("BSV_OUT_PASSWORD");
        $this->walletUnlockTime = env("BSV_UNLOCK_TIME");
        $this->uri=$this->nodePort?$this->nodeIp . ":" . $this->nodePort:$this->nodeIp;
    }
}