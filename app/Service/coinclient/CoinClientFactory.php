<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 12
 * Time: 오후 6:48
 */

namespace App\Service\coinclient;

use App\Exceptions\ApplicationException;

class CoinClientFactory
{
    private $clientPool = [];
    /**
     * @param $coinSymbol
     * @return mixed
     * @throws ApplicationException
     */
    private function createClient($coinSymbol): CoinClientInterface
    {
        $client = "App\Service\coinclient\\".$coinSymbol."Client";
        if (class_exists($client)) {
            if (!isset($this->clientPool[$coinSymbol])) {
                $this->clientPool[$coinSymbol] = new $client;
            }
            return $this->clientPool[$coinSymbol];
        } else {
            throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
        }
    }

    /**
     * @param $currency
     * @return mixed
     * @throws ApplicationException
     */
    public function getClient($currency)
    {
        if (is_null($currency->parent)) {
            $client = $this->createClient($currency->symbol);
        } else {
            $client = $this->createClient($currency->parent->symbol);
        }
        return $client;
    }
}