<?php


namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;

class BTCClient implements CoinClientInterface
{
    protected $nodeIp;
    protected $nodePort;
    protected $nodeUser;
    protected $nodePassword;
    protected $walletPass;
    protected $walletUnlockTime;
    protected $uri;

    /**
     * BCHClient constructor.
     */
    public function __construct()
    {
        $this->nodeIp = env("BTC_NODE_IP");
        $this->nodePort = env("BTC_NODE_PORT");
        $this->nodeUser = env("BTC_NODE_USER");
        $this->nodePassword = env("BTC_NODE_PASSWORD");
        $this->walletPass = env("BTC_OUT_PASSWORD");
        $this->walletUnlockTime = env("BTC_UNLOCK_TIME");
        $this->uri=$this->nodePort?$this->nodeIp . ":" . $this->nodePort:$this->nodeIp;
    }

    public function createCoinAddr()
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $req = new JsonRpcRequest('getnewaddress', rand(1, 9999), []);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::CREATE_COIN_ADDR_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function send($currency, $attributes)
    {
        if (!empty($this->walletPass)) {
            $this->unlock('', $this->walletPass, intval($this->walletUnlockTime));
        }
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $req = new JsonRpcRequest('sendtoaddress', rand(1, 9999), $attributes);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function getTransactionReceipt($txid)
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $req = new JsonRpcRequest('gettransaction', rand(1, 9999), [$txid]);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::RECEIPT_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function getBlock($blockNumber, $endBlockNum=null)
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $req = new JsonRpcRequest('getblock', rand(1, 9999), [$blockNumber]);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function getListBlock($blockHash)
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        if (is_null($blockHash)) {
            $params = [];
        } else {
            $params = [$blockHash];
        }
        $req = new JsonRpcRequest('listsinceblock', rand(1, 9999), $params);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_LIST_BLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function getBlockHeight()
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $req = new JsonRpcRequest('getblockchaininfo', rand(1, 9999), []);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_HEIGHT_ERR, print_r($res["error"], true));
        }
        return $res["result"]["headers"];
    }

    public function getBalance()
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $req = new JsonRpcRequest('getbalance', rand(1, 9999), []);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::GET_BALANCE_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function unlock($addr, $password, $secends)
    {
        $cli = new JsonRpcClient($this->uri, [
            'headers' => ['Content-Type'=>'application/json'],
            'auth' => [$this->nodeUser, $this->nodePassword],
        ]);
        $req = new JsonRpcRequest('walletpassphrase', rand(1, 9999), [$password, $secends]);
        $res = $cli->appendRequest($req)->send();
        if(isset($res["error"]) && !is_null($res["error"])) {
            throw new DaemonException(DaemonException::UNLOCK_ERR, print_r($res["error"], true));
        }
        return $res["result"];
    }

    public function lock()
    {
        // TODO: Implement lock() method.
    }
}