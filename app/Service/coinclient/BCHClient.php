<?php


namespace App\Service\coinclient;


use App\Exceptions\DaemonException;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;

class BCHClient extends BTCClient
{
    protected $nodeIp;
    protected $nodePort;
    protected $nodeUser;
    protected $nodePassword;
    protected $uri;

    /**
     * BCHClient constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->nodeIp = env("BCH_NODE_IP");
        $this->nodePort = env("BCH_NODE_PORT");
        $this->nodeUser = env("BCH_NODE_USER");
        $this->nodePassword = env("BCH_NODE_PASSWORD");
        $this->walletPass = env("BCH_OUT_PASSWORD");
        $this->walletUnlockTime = env("BCH_UNLOCK_TIME");
        $this->uri=$this->nodePort?$this->nodeIp . ":" . $this->nodePort:$this->nodeIp;
    }
}