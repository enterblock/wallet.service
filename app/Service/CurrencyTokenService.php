<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 9. 26
 * Time: 오후 6:43
 */

namespace App\Service;


use App\Models\Currency;
use App\Repositories\CurrencyTokenRepository;

class CurrencyTokenService
{
    private $currencyTokenRepo;

    public function __construct(CurrencyTokenRepository $currencyTokenRepo)
    {
        $this->currencyTokenRepo = $currencyTokenRepo;
    }

    public function all()
    {
        return $this->currencyTokenRepo->all();
    }

    public function find($id)
    {
        return $this->currencyTokenRepo->find($id);
    }

    public function update($id, $attributes)
    {
        return $this->currencyTokenRepo->find($id)->update($attributes);
    }

    public function delete($id)
    {
        return $this->currencyTokenRepo->find($id)->delete();
    }

    /**
     * @param $tokenId
     * @return Currency mixed
     */
    public function getCurrencyToken($tokenId)
    {
        return $this->currencyTokenRepo->where(['token_id'=>$tokenId]);
    }
}