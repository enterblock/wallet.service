<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 28
 * Time: 오후 8:32
 */

namespace App\Service\coinDaemonLog;


use App\Exceptions\ApplicationException;
use App\Repositories\DaemonTxRepository;
use App\Service\coinclient\ETHClient;
use App\Utils\StringUtils;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ETHTokenDaemonLogService extends CoinDaemonLogAbstract
{
    protected $client;
    protected $coinSymbol = 'ETHToken';
    protected $parentSymbol = 'ETH';

    const TRANSFER_TOPIC = '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef';

    public function getClient()
    {
        return new ETHClient();
    }


    public function getBlock($blockNumber)
    {
        return $this->client->getBlock($blockNumber);
    }

    public function getSearchPointCurrencyId()
    {
        return 2000000000;
    }

    /**
     * @param $dsp
     * @param $symbol
     * @return array
     */
    public function getTransactions($dsp, $symbol): array
    {
        $latestBlocknumber = $this->getBlockHeight();
        $latestBlocknumber = $latestBlocknumber - 10;

        $startBlock = is_null($dsp->point) ? 1 : $dsp->point;
        $endBlock = $dsp->point + $this->searchCnt;
        $endBlock = min($latestBlocknumber, $endBlock);
        $procDaemonTxs = [];
        for ($i = $startBlock; $i < $endBlock; $i++) {
            $block = $this->getBlock($i);
            $blocknumber = $i;
            $blockhash = $block['hash'];
            Log::debug("$this->coinSymbol 처리중 : $i");
            foreach ($block['transactions'] as $transaction) {
                try {
                    // eth 수량이 0인것들 continue
                    if (StringUtils::bcHexdec($transaction['value'])>0) {
                        continue;
                    }

                    // 거래소에서 취급하는 토큰인지 확인
                    $currencyToken = $this->getCurrencyToken($transaction['to'])->get();
                    if($currencyToken->count()<1) {
                        continue;
                    }
                    if($currencyToken->count()>1) {
                        Log::emergency('중복된 contract 주소 : DB체크 필요.');
                        continue;
                    }
                    $currencyToken = $currencyToken->first();

                    $currencyInfo = $this->currencyService->getCurrency($currencyToken->currency_id);
                    if (is_null($currencyInfo->parent)) {
                        // USDT 컨트랙트 이슈로 예외처리
                        if ($this->coinSymbol != 'USDT') {
                            // Log::debug("메인넷 제외 : $currencyInfo->symbol");
                            continue;
                        }
                    }

                    // receipt 확인
                    $txReceipt = $this->client->getTransactionReceipt($transaction['hash']);

                    foreach ($txReceipt['logs'] as $eventLog) {
                        $sender = '';
                        $receiver = '';
                        $amount = '';
                        $type = '';
                        if (count($eventLog['topics'])!=3 || current($eventLog['topics'])!=self::TRANSFER_TOPIC) {
                            continue;
                        }

                        $txIndex = StringUtils::bcHexdec($eventLog['logIndex']);
                        if ($this->checkDaemonTx($currencyToken->currency_id, $transaction['hash'], $txIndex)) {
                            Log::debug("이미 처리된 거래.");
                            continue;
                        }

                        $decimalPoint = $currencyInfo->decimal_point;
                        if (empty($decimalPoint) || is_null($decimalPoint)) {
                            Log::emergency("decimal point 이상!");
                            continue;
                        }

                        $sender = '0x' . substr($eventLog['topics'][1], -40);
                        $receiver = '0x' . substr($eventLog['topics'][2], -40);
                        $amount = bcdiv(StringUtils::bcHexdec($eventLog['data']),bcpow('10',$decimalPoint),$decimalPoint);

                        $daemonTxAttributes = [
                            'hash' => $transaction['hash'],
                            'currency_id' => $currencyToken->currency_id,
                            'sender' => $sender,
                            'receiver' => $receiver,
                            'amount' => $amount,
                            'tx_index' => $txIndex,
                            'blockhash' => $blockhash,
                            'blocknumber' => $blocknumber,
                            'block_time' => Carbon::createFromTimestamp(hexdec($block['timestamp'])),
                            'status' => DaemonTxRepository::STATUS_WAIT,
                        ];
                        $type = $this->getType($currencyToken->currency_id, $daemonTxAttributes['sender'], $daemonTxAttributes['receiver']);
                        if ($type == self::TYPE_TX_EXTERNAL) { // 거래소와 관계없는 주소일 경우 continue
                            // Log::debug("외부 입출금 continue");
                            continue;
                        } else if ($type == self::TYPE_TX_INTERNAL) { // 내부의 경우 입/출금 모두 insert
                            $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_WITHDRAW_INTERNAL;
                            $procDaemonTxs[] = $daemonTxAttributes;
                            $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_DEPOSIT_INTERNAL;
                            $procDaemonTxs[] = $daemonTxAttributes;
                        } else {
                            $daemonTxAttributes['type'] = $type;
                            $procDaemonTxs[] = $daemonTxAttributes;
                        }
                    }
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    continue;
                }
            }
        }
        $this->endPoint = $endBlock;
        return $procDaemonTxs;
    }

    protected function getBlockHeight()
    {
        return StringUtils::bcHexdec($this->client->getBlockHeight());
    }

    function getSearchCnt()
    {
        return env('ETH_DAEMON_ONCE_SEARCH_CNT', 10);
    }

    function getCurrencyToken($tokenId)
    {
        return $this->currencyTokenService->getCurrencyToken($tokenId);
    }

    /**
     * 대기중 transaction 조회
     * 토큰은 계열별로 처리
     * @return mixed
     */
    public function getWaitDaemonTx()
    {
        $currency = $this->getCurrency($this->parentSymbol);
        $erc20CurrencyId = $currency->children->pluck('id')->toArray();
        $daemonTxs = $this->daemonTxRepository->whereIn('currency_id', $erc20CurrencyId)->where(['status'=>DaemonTxRepository::STATUS_WAIT])->get();
        $daemonTxRepository = $this->daemonTxRepository;
        $updateDaemonTxs = $daemonTxs->filter(function ($daemonTx) use ($daemonTxRepository) {
            return $daemonTxRepository->where(['id' => $daemonTx->id, 'status' => DaemonTxRepository::STATUS_WAIT])->update(['status' => DaemonTxRepository::STATUS_ING]);
        });
        return $updateDaemonTxs;
    }

    /**
     * @param $daemonTx
     * @throws ApplicationException
     */
    public function checkProcess($currency, $daemonTx)
    {
        $decimalPoint = $currency->decimal_point;
        $txReceipt = $this->client->getTransactionReceipt($daemonTx->hash);
        $currentHeight = $this->getBlockHeight();
        $txHeight = StringUtils::bcHexdec($txReceipt['blockNumber']);
        $confirm = $currentHeight-$txHeight;

        $stdConfirms = $this->getStdConfirms($daemonTx->currency_id);
        if($txReceipt['status']=="0x1") {
            if ($stdConfirms <= $confirm) {
                Log::channel('slack_tx_info')->info("txid 확인 \n- 타입: ".$daemonTx->type." \n- txid: ".$daemonTx->hash." \n- currency: ".$daemonTx->currency->symbol." \n- amount:" . $daemonTx->amount);
                $txInfo = $this->client->getTransaction($daemonTx->hash);
                $daemonTx->status = DaemonTxRepository::STATUS_SUCCESS;
                $daemonTx->confirm = $confirm;
                $daemonTx->fee = bcmul(bcdiv(StringUtils::bcHexdec($txInfo['gasPrice']), bcpow('10', 18), 18),StringUtils::bcHexdec($txReceipt['gasUsed']), 18);
            } else {
                $this->daemonTxRepository->update($daemonTx->id,['status' => DaemonTxRepository::STATUS_WAIT, 'confirm'=>$confirm]);
            }
        } else {
            $daemonTx->status = DaemonTxRepository::STATUS_FAIL;
            $daemonTx->confirm = $confirm;
        }
        $daemonTx->save();
    }

    protected function getTransactionsByBlock($symbol, $blockNumber): array
    {
        $procDaemonTxs = [];
        $block = $this->getBlock($blockNumber);
        $blockhash = $block['hash'];
        Log::debug("ETH token 처리중 : $blockNumber");
        foreach ($block['transactions'] as $transaction) {
            try {
                // eth 수량이 0인것들 continue
                if (StringUtils::bcHexdec($transaction['value'])>0) {
                    continue;
                }

                // 거래소에서 취급하는 토큰인지 확인
                $currencyToken = $this->getCurrencyToken($transaction['to']);
                if(!$currencyToken->isNotEmpty()) {
                    continue;
                }
                if($currencyToken->count()>1) {
                    Log::error('중복된 contract 주소 : DB체크 필요.');
                    continue;
                }
                $currencyToken = $currencyToken->first();

                // receipt 확인
                $txReceipt = $this->client->getTransactionReceipt($transaction['hash']);

                foreach ($txReceipt['logs'] as $eventLog) {
                    $sender = '';
                    $receiver = '';
                    $amount = '';
                    $type = '';
                    if (count($eventLog['topics'])!=3 || current($eventLog['topics'])!=self::TRANSFER_TOPIC) {
                        continue;
                    }

                    $txIndex = StringUtils::bcHexdec($eventLog['logIndex']);
                    if ($this->checkDaemonTx($currencyToken->currency_id, $transaction['hash'], $txIndex)) {
                        Log::debug("이미 처리된 거래.");
                        continue;
                    }

                    $decimalPoint = $currencyToken->currency->decimal_point;

                    $sender = '0x' . substr($eventLog['topics'][1], -40);
                    $receiver = '0x' . substr($eventLog['topics'][2], -40);
                    $amount = bcdiv(StringUtils::bcHexdec($eventLog['data']),bcpow('10',$decimalPoint),$decimalPoint);

                    $daemonTxAttributes = [
                        'hash' => $transaction['hash'],
                        'currency_id' => $currencyToken->currency_id,
                        'sender' => $sender,
                        'receiver' => $receiver,
                        'amount' => $amount,
                        'tx_index' => $txIndex,
                        'blockhash' => $blockhash,
                        'blocknumber' => $blockNumber,
                        'block_time' => Carbon::createFromTimestamp(hexdec($block['timestamp'])),
                        'status' => DaemonTxRepository::STATUS_WAIT,
                    ];
                    $type = $this->getType($currencyToken->currency_id, $daemonTxAttributes['sender'], $daemonTxAttributes['receiver']);
                    if ($type == self::TYPE_TX_EXTERNAL) { // 거래소와 관계없는 주소일 경우 continue
                        Log::debug("외부 입출금 continue");
                        continue;
                    } else if ($type == self::TYPE_TX_INTERNAL) { // 내부의 경우 입/출금 모두 insert
                        $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_WITHDRAW_INTERNAL;
                        $procDaemonTxs[] = $daemonTxAttributes;
                        $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_DEPOSIT_INTERNAL;
                        $procDaemonTxs[] = $daemonTxAttributes;
                    } else {
                        $daemonTxAttributes['type'] = $type;
                        $procDaemonTxs[] = $daemonTxAttributes;
                    }
                }
            } catch (Exception $e) {
                Log::error($e->getMessage());
                continue;
            }
        }
        return $procDaemonTxs;
    }

    protected function getInternalAddresses()
    {
        return [env('ETH_OUT_ADDRESS')];
    }
}
