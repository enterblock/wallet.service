<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 28
 * Time: 오후 8:32
 */

namespace App\Service\coinDaemonLog;

use App\Exceptions\ApplicationException;
use App\Exceptions\DaemonException;
use App\Repositories\DaemonTxRepository;
use App\Service\coinclient\XRPClient;
use App\Service\coinDeposit\CoinDepositService;
use App\Utils\StringUtils;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class XRPDaemonLogService extends CoinDaemonLogAbstract
{
    protected $client;
    protected $coinSymbol = 'XRP';

    const XRP_STATUS_SUCCESS = "success";
    const XRP_TRANSACTION_RESULT_RETRY_CODE = ['ter', 'tel'];
    const XRP_TRANSACTION_RESULT_FAIL_CODE = ['tec', 'tef', 'tem'];
    const XRP_TRANSACTION_RESULT_SUCCESS_CODE = ['tes'];

    public function getClient()
    {
        return new XRPClient();
    }


    public function getBlockHeight()
    {
        $current = $this->client->getBlockHeight();
        if ($current["status"] != self::XRP_STATUS_SUCCESS && empty($current["ledger_current_index"])) {
            throw new DaemonException(DaemonException::GET_BLOCK_ERR);
        }
        return $current["ledger_current_index"];
    }

    /**
     * @param $dsp
     * @param $symbol
     * @return array
     * @throws DaemonException
     */
    public function getTransactions($dsp, $symbol): array
    {
        $latestBlocknumber = $this->getBlockHeight();
        $startBlock = is_null($dsp->point) ? 1 : $dsp->point;
        $endBlock = $dsp->point + $this->searchCnt;
        $procDaemonTxs = [];
        $currency = $this->getCurrency($this->coinSymbol);
        $decimalPoint = $currency->decimal_point;

        if($latestBlocknumber>=$endBlock) {
            Log::debug("XRP 처리중 : $startBlock");
            $block = $this->client->getBlock($startBlock);
        } else {
            Log::debug("XRP 처리중 : $startBlock / $endBlock");
            $block = $this->client->getBlock($startBlock, $endBlock);
        }
        if (!isset($block['transactions'])) {
            $this->endPoint = $endBlock;
            return [];
        }
        foreach ($block['transactions'] as $transaction) {
            if ($transaction['tx']['TransactionType'] != 'Payment') {
                continue;
            }

            if (!is_numeric($transaction['tx']['Amount']) || $transaction['tx']['Amount']<=0) {
                continue;
            }

            $blockNumber = $transaction['tx']['ledger_index'];
            $blockhash = $transaction['tx']['ledger_index'];

            if ($this->checkDaemonTx($currency->id, $transaction['tx']['hash'])) {
                Log::debug("이미 처리된 거래.");
                continue;
            }
            $destinationTag = null;
            if (isset($transaction['tx']['DestinationTag'])) {
                $destinationTag = $transaction['tx']['DestinationTag'];
            }
            $daemonTxAttributes = [
                'hash' => $transaction['tx']['hash'],
                'currency_id' =>  $currency->id,
                'sender' => $transaction['tx']['Account'],
                'receiver' => $transaction['tx']['Destination'],
                'receiver_sub' => $destinationTag,
                'amount' => bcdiv($transaction['tx']['Amount'], bcpow('10', $decimalPoint), $decimalPoint),
                'blockhash' => $blockhash,
                'blocknumber' => $blockNumber,
                'block_time' => Carbon::createFromTimestamp(StringUtils::rippleToUnixTimestamp($transaction['tx']['date'])),
                'status' => DaemonTxRepository::STATUS_WAIT,
            ];
            $type = $this->getType($currency->id, $daemonTxAttributes['sender'], $daemonTxAttributes['receiver']);
            if ($type == self::TYPE_TX_EXTERNAL) { // 거래소와 관계없는 주소일 경우 continue
                continue;
            } else if ($type == self::TYPE_TX_INTERNAL) { // 내부의 경우 입/출금 모두 insert
                $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_WITHDRAW_INTERNAL;
                $procDaemonTxs[] = $daemonTxAttributes;
                $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_DEPOSIT_INTERNAL;
                $procDaemonTxs[] = $daemonTxAttributes;
            } else if($type == DaemonTxRepository::TYPE_WITHDRAW) {
                $daemonTxAttributes['type'] = $type;
                // 출금 데이터이므로 status_deposit 는 성공으로 update
                $daemonTxAttributes['status_deposit'] = CoinDepositService::STATUS_DEPOSIT_SUCCESS;
                $procDaemonTxs[] = $daemonTxAttributes;
            } else {
                $daemonTxAttributes['type'] = $type;
                $procDaemonTxs[] = $daemonTxAttributes;
            }
        }

        if($latestBlocknumber>=$endBlock) {
            $this->endPoint = $block["ledger_index_max"];
        } else {
            $this->endPoint = $endBlock;
        }
        return $procDaemonTxs;
    }

    public function getSearchCnt()
    {
        return env('ETH_DAEMON_ONCE_SEARCH_CNT', 10);
    }

    /**
     * @param $daemonTx
     * @throws ApplicationException
     * @throws DaemonException
     */
    public function checkProcess($currency, $daemonTx)
    {
        $txReceipt = $this->client->getTransactionReceipt($daemonTx->hash);
        $decimalPoint = $currency->decimal_point;
        if(!$txReceipt['validated'] || $txReceipt['status']!==self::XRP_STATUS_SUCCESS) {
            throw new ApplicationException(ApplicationException::ABNORMAL_DATA);
        }
        $currentHeight = $this->getBlockHeight();
        $txHeight = $txReceipt['ledger_index'];
        $confirm = $currentHeight-$txHeight;

        $currencyCt = $this->ctRepository->where(['currency_id'=>$daemonTx->currency_id])->first();
        $stdConfirms = $currencyCt->confirms;
        if ($stdConfirms <= 0) {
            throw new ApplicationException(ApplicationException::ABNORMAL_DATA);
        }
        if ($stdConfirms <= $confirm) {
            $txResultPrefix = substr($txReceipt['meta']['TransactionResult'], 0, 3);
            $daemonTx->confirm = $confirm;
            $daemonTx->fee = bcdiv($txReceipt['Fee'], bcpow('10', $decimalPoint), $decimalPoint);

            if (in_array($txResultPrefix, self::XRP_TRANSACTION_RESULT_FAIL_CODE)) {
                Log::channel('slack_tx_info')->error("txid 확인 \n- 타입: ".$daemonTx->type." \n- txid: ".$daemonTx->hash." \n- currency: ".$daemonTx->currency->symbol." \n- amount:" . $daemonTx->amount);
                $daemonTx->status = DaemonTxRepository::STATUS_FAIL;
            } else if (in_array($txResultPrefix, self::XRP_TRANSACTION_RESULT_RETRY_CODE)) {
                $daemonTx->status = DaemonTxRepository::STATUS_WAIT;
            } else if (in_array($txResultPrefix, self::XRP_TRANSACTION_RESULT_SUCCESS_CODE)) {
                Log::channel('slack_tx_info')->info("txid 확인 \n- 타입: ".$daemonTx->type." \n- txid: ".$daemonTx->hash." \n- currency: ".$daemonTx->currency->symbol." \n- amount:" . $daemonTx->amount);
                $daemonTx->status = DaemonTxRepository::STATUS_SUCCESS;
            } else {
                Log::channel('slack_tx_info')->error("txid 확인 \n- 타입: ".$daemonTx->type." \n- txid: ".$daemonTx->hash." \n- currency: ".$daemonTx->currency->symbol." \n- amount:" . $daemonTx->amount);
                $daemonTx->status = DaemonTxRepository::STATUS_FAIL;
            }
        } else {
            $daemonTx->status = DaemonTxRepository::STATUS_WAIT;
            $daemonTx->confirm = $confirm;
        }
        $daemonTx->save();
    }

    protected function getTransactionsByBlock($symbol, $blockNumber): array
    {
        // TODO: Implement getTransactionsByBlock() method.
    }

    protected function getInternalAddresses()
    {
        return [];
    }
}