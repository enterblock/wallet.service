<?php


namespace App\Service\coinDaemonLog;

use App\Repositories\DaemonTxRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use function Psy\debug;

class USDTBTCDaemonLogService extends BTCDaemonLogService
{
    const VALID_USDT_TYPE = 'Simple Send';
    const USDT_PROPERTY_ID = 31;
    protected $coinSymbol = 'USDT';

    protected function getTransactions($dsp, $symbol): array
    {
        $blockHeight = $this->getBlockHeight();
        $blockHeight = $blockHeight - 10;
        Log::debug("{$this->coinSymbol} 처리중 : $dsp->point / $blockHeight");
        $txList = $this->client->getListBlock($dsp->point, $blockHeight);
        $currency = $this->getCurrency($this->coinSymbol);
        $procDaemonTxs = [];
        foreach ($txList as $transaction) {
            if (!$transaction['valid'] || !$transaction['ismine']) {
                continue;
            }
            if ($transaction['propertyid'] != self::USDT_PROPERTY_ID) {
                continue;
            }
            if ($transaction['type'] != self::VALID_USDT_TYPE) {
                Log::debug('트랜잭션 타입이 ' . self::VALID_USDT_TYPE . '이 아님.');
                continue;
            }
            if ($this->checkDaemonTxBTC($currency->id, $transaction['txid'], null, $this->getType($currency->id, $transaction['sendingaddress'], $transaction['referenceaddress']))) {
                Log::debug("이미 처리된 거래.");
                continue;
            }
            $blockInfo = $this->client->getBlock($transaction['blockhash']);
            $daemonTxAttributes = [
                'hash' => $transaction['txid'],
                'currency_id' =>  $currency->id,
                'sender' => $transaction['sendingaddress'],
                'receiver' => $transaction['referenceaddress'],
                'amount' => abs($transaction['amount']),
                'blockhash' => $transaction['blockhash'],
                'blocknumber' => $blockInfo['height'],
                'block_time' => Carbon::createFromTimestamp($blockInfo['time']),
                'status' => DaemonTxRepository::STATUS_WAIT,
                'type' => $this->getType($currency->id, $transaction['sendingaddress'], $transaction['referenceaddress']),
            ];
            $procDaemonTxs[] = $daemonTxAttributes;
        }
        $this->endPoint = $blockHeight;
        return $procDaemonTxs;
    }
}