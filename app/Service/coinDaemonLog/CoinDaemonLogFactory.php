<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 28
 * Time: 오후 8:32
 */

namespace App\Service\coinDaemonLog;


use App\Exceptions\ApplicationException;
use App\Service\CurrencyService;


class CoinDaemonLogFactory
{
    private $currencyService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    /**
     * @param $symbol
     * @return CoinDaemonLogAbstract
     * @throws ApplicationException
     */
    public function build($symbol): CoinDaemonLogAbstract
    {
        $client = $symbol."DaemonLogService";
        if (app()->bound($client)) {
            return resolve($client);
        } else {
            // symbol 확인
            $currency = $this->currencyService->findBySymbol($symbol);
            if (!is_null($currency) && !is_null($currency->parent)) {
                $client = $currency->parent->symbol."TokenDaemonLogService";
                if (app()->bound($client)) {
                    return resolve($client);
                }
            }
            throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
        }
    }
}