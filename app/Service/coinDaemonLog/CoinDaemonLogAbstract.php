<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 28
 * Time: 오후 8:31
 */

namespace App\Service\coinDaemonLog;


use App\Exceptions\ApplicationException;
use App\Exceptions\JobRetryException;
use App\Models\DaemonTx;
use App\Repositories\CoinMergeRepository;
use App\Repositories\CurrencyCtRepository;
use App\Repositories\DaemonSearchPointRepository;
use App\Repositories\DaemonTxRepository;
use App\Service\coinclient\CoinClientFactory;
use App\Service\CurrencyService;
use App\Service\CurrencyTokenService;
use App\Service\CurrencyUserService;
use App\Utils\StringUtils;
use Illuminate\Support\Facades\Log;

abstract class CoinDaemonLogAbstract
{
    protected $dspRepository;
    protected $currencyService;
    protected $currency;
    protected $client;
    protected $coinSymbol;
    protected $searchCnt;
    protected $decimalPoint;
    protected $endPoint;
    protected $currencyUserService;
    protected $daemonTxRepository;
    protected $currencyTokenService;
    protected $ctRepository;
    protected $internalAddresses;
    protected $coinClientFactory;
    protected $coinMergeRepository;

    const TYPE_PREFIX_POINT_DAEMON_LOG = 'log_';
    const TYPE_TX_INTERNAL = 'internal';
    const TYPE_TX_EXTERNAL = 'external';

    const BTC_TYPE_RECEIVE = 'receive';
    const BTC_TYPE_SEND = 'send';

    /**
     * CoinDaemonLogAbstract constructor.
     * @param CurrencyService $currencyService
     * @param DaemonSearchPointRepository $dspRepository
     * @param CurrencyUserService $currencyUserService
     * @param DaemonTxRepository $daemonTxRepository
     * @param CurrencyTokenService $currencyTokenService
     * @param CurrencyCtRepository $ctRepository
     * @param CoinClientFactory $coinClientFactory
     * @param CoinMergeRepository $coinMergeRepository
     */
    public function __construct(CurrencyService $currencyService, DaemonSearchPointRepository $dspRepository,
                                CurrencyUserService $currencyUserService, DaemonTxRepository $daemonTxRepository,
                                CurrencyTokenService $currencyTokenService, CurrencyCtRepository $ctRepository,
                                CoinClientFactory $coinClientFactory, CoinMergeRepository $coinMergeRepository)
    {
        $this->dspRepository = $dspRepository;
        $this->currencyService = $currencyService;
        $this->currencyUserService = $currencyUserService;
        $this->daemonTxRepository = $daemonTxRepository;
        $this->currencyTokenService = $currencyTokenService;
        $this->ctRepository = $ctRepository;
        $this->coinClientFactory = $coinClientFactory;
        $this->coinMergeRepository = $coinMergeRepository;
        $this->client = $this->getClient();
        $this->searchCnt = $this->getSearchCnt();
        $this->internalAddresses = $this->getInternalAddresses();
    }

    protected function getClient()
    {
        $currency = $this->currencyService->findBySymbol($this->coinSymbol);
        return $this->coinClientFactory->getClient($currency);
    }

    abstract protected function getBlockHeight();

    abstract protected function getTransactions($dsp, $symbol): array;

    abstract protected function getTransactionsByBlock($symbol, $blockNumber): array;

    abstract protected function getSearchCnt();

    abstract protected function getInternalAddresses();

    /**
     * 대기중 transaction 조회
     * @return mixed
     */
    public function getWaitDaemonTx()
    {
        $currency = $this->getCurrency($this->coinSymbol);
        $daemonTxs = $this->daemonTxRepository->where(['currency_id'=>$currency->id,'status'=>DaemonTxRepository::STATUS_WAIT])->get();
        $daemonTxRepository = $this->daemonTxRepository;
        $updateDaemonTxs = $daemonTxs->filter(function ($daemonTx) use ($daemonTxRepository) {
            return $daemonTxRepository->where(['id' => $daemonTx->id, 'status' => DaemonTxRepository::STATUS_WAIT])->update(['status' => DaemonTxRepository::STATUS_ING]);
        });
        return $updateDaemonTxs;
    }

    /**
     * 트랜잭션 검증 로직
     * @param $daemonTx
     * @throws ApplicationException
     */
    abstract public function checkProcess($currency, $daemonTx);

    protected function getSearchPoint($currencyId, $type)
    {
        $params = [
            'currency_id' => $currencyId,
            'type' => self::TYPE_PREFIX_POINT_DAEMON_LOG . $type
        ];
        return $this->dspRepository->firstOrCreate($params);
    }

    protected function getCurrency($symbol)
    {
        return $this->currencyService->findBySymbol($symbol);
    }

    public function process($type, $symbol)
    {
        $searchPointCurrencyId = $this->getSearchPointCurrencyId();
        /*
         * 데몬조회 포인트 조회.
         */
        $dsp = $this->getSearchPoint($searchPointCurrencyId, $type);

        // 트랜잭션 검증
        $procDaemonTxs = $this->getTransactions($dsp, $symbol);
        if (count($procDaemonTxs) > 0) {
            Log::info('proc tx : ' . print_r($procDaemonTxs, true));
        }
//        $daemonTxs = [];

        // DB update or insert
        foreach ($procDaemonTxs as $daemonTx) {
            $this->createDaemonTx($daemonTx);
        }

//        if (count($daemonTxs) > 0) {
//            DaemonLogReceiptJob::dispatch($daemonTxs);
//        }

        $this->updateSearchPoint($searchPointCurrencyId, $type);

    }

    public function processOnce($symbol, $blockNumber)
    {
        // 트랜잭션 검증
        $procDaemonTxs = $this->getTransactionsByBlock($symbol, $blockNumber);
        // DB update or insert
        foreach ($procDaemonTxs as $daemonTx) {
            $this->daemonTxRepository->create($daemonTx);
        }

    }

    public function checkDaemonTx($currencyId, $hash, $txIndex=null)
    {
        $where = is_null($txIndex) ? ['currency_id'=>$currencyId, 'hash'=>$hash] : ['currency_id'=>$currencyId, 'hash'=>$hash, 'tx_index'=>$txIndex];
        $daemonTx = $this->daemonTxRepository->where($where)->get();
        return $daemonTx->isNotEmpty();
    }

    public function getSearchPointCurrencyId()
    {
        $currency = $this->getCurrency($this->coinSymbol);
        return $currency->id;
    }

    private function updateSearchPoint($currencyId, $type)
    {
        $searchAttr = [
            'currency_id' => $currencyId,
            'type' => self::TYPE_PREFIX_POINT_DAEMON_LOG . $type
        ];

        $setAttr = [
            'point' => $this->endPoint
        ];
        $this->dspRepository->updateOrCreate($searchAttr, $setAttr);
    }

    /**
     * @param $currencyId
     * @return mixed
     * @throws ApplicationException
     */
    protected function getStdConfirms($currencyId)
    {
        $currencyCt = $this->ctRepository->where(['currency_id' => $currencyId])->first();
        if (!isset($currencyCt->confirms) || $currencyCt->confirms <= 0) {
            throw new ApplicationException(ApplicationException::ABNORMAL_DATA);
        }
        $stdConfirms = $currencyCt->confirms;
        return $stdConfirms;
    }

    protected function getType($currencyId, $sender, $receiver)
    {
        $type = null;

        $senderUserAddr = $this->currencyUserService->findByAddr($currencyId, $sender);
        $receiverUserAddr = $this->currencyUserService->findByAddr($currencyId, $receiver);

        // 아래 검증 순서도 중요 변경시 주의 요망
        if (!is_null($senderUserAddr) && in_array($receiver,$this->internalAddresses)) { // 보내는 주소가 내부, 받는주소가 출금주소일 경우.
            $type = DaemonTxRepository::TYPE_MERGE;
        } else if (in_array($sender,$this->internalAddresses) && !is_null($receiverUserAddr)) { // 보내는 주소가 출금주소, 받는주소가 내부일 경우.
            $type = DaemonTxRepository::TYPE_FEE;
        } else if(!is_null($senderUserAddr) && !is_null($receiverUserAddr)) { // 보내는 주소 / 받는 주소가 내부일 경우.
            $type = self::TYPE_TX_INTERNAL;
        } else if (is_null($senderUserAddr) && (!is_null($receiverUserAddr) || in_array($receiver,$this->internalAddresses))){ // 보내는 주소가 외부, 받는주소가 내부일 경우.
            $type = DaemonTxRepository::TYPE_DEPOSIT;
        } else if ((!is_null($senderUserAddr) || in_array($sender,$this->internalAddresses)) && is_null($receiverUserAddr)) { // 보내는 주소가 내부 or 출금주소, 받는주소가 외부일 경우
            $type = DaemonTxRepository::TYPE_WITHDRAW;
        } else { // 관계없는 트랜잭션
            $type = self::TYPE_TX_EXTERNAL;
        }

        return $type;
    }

    /**
     * @param $daemonTx
     * @return DaemonTx
     */
    private function createDaemonTx($daemonTx)
    {
        $searchParams = [
            'currency_id' => $daemonTx['currency_id'],
            'type' => $daemonTx['type'],
            'hash' => $daemonTx['hash'],
        ];
        if (isset($daemonTx['tx_index']) && !is_null($daemonTx['tx_index'])) {
            $searchParams['tx_index'] = $daemonTx['tx_index'];
        }
        if (isset($daemonTx['receiver_sub']) && !is_null($daemonTx['receiver_sub'])) {
            $searchParams['receiver_sub'] = $daemonTx['receiver_sub'];
        }
        return $this->daemonTxRepository->updateOrCreate($searchParams, $daemonTx);
    }
}
