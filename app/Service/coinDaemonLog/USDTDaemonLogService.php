<?php


namespace App\Service\coinDaemonLog;

use App\Repositories\DaemonTxRepository;

class USDTDaemonLogService extends ETHTokenDaemonLogService
{
    protected $coinSymbol = 'USDT';

    public function getSearchPointCurrencyId()
    {
        $currency = $this->getCurrency($this->coinSymbol);
        return $currency->id;
    }

    /**
     * 대기중 transaction 조회
     * @return mixed
     */
    public function getWaitDaemonTx()
    {
        $currency = $this->getCurrency($this->coinSymbol);
        $daemonTxs = $this->daemonTxRepository->where(['currency_id'=>$currency->id,'status'=>DaemonTxRepository::STATUS_WAIT])->get();
        $daemonTxRepository = $this->daemonTxRepository;
        $updateDaemonTxs = $daemonTxs->filter(function ($daemonTx) use ($daemonTxRepository) {
            return $daemonTxRepository->where(['id' => $daemonTx->id, 'status' => DaemonTxRepository::STATUS_WAIT])->update(['status' => DaemonTxRepository::STATUS_ING]);
        });
        return $updateDaemonTxs;
    }

    function getSearchCnt()
    {
        return env('USDT_DAEMON_ONCE_SEARCH_CNT', 10);
    }
}