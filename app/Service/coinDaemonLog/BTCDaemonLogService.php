<?php


namespace App\Service\coinDaemonLog;


use App\Exceptions\ApplicationException;
use App\Exceptions\DaemonException;
use App\Repositories\DaemonTxRepository;
use App\Utils\Http\JsonRpcClient;
use App\Utils\Http\JsonRpcRequest;
use App\Utils\StringUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class BTCDaemonLogService extends CoinDaemonLogAbstract
{
    protected $client;
    protected $coinSymbol = 'BTC';

    protected function getBlockHeight()
    {
        $blockHeight = $this->client->getBlockHeight();
        return $blockHeight;
    }

    protected function getTransactions($dsp, $symbol): array
    {
        Log::debug("{$this->coinSymbol} 처리중 : $dsp->point");
        $txList = $this->client->getListBlock($dsp->point);
        $currency = $this->getCurrency($this->coinSymbol);
        $procDaemonTxs = [];
        foreach ($txList['transactions'] as $transaction) {
            if ($this->checkDaemonTxBTC($currency->id, $transaction['txid'], $transaction['vout'], $this->getTypeBTC($transaction['category']))) {
                Log::debug("이미 처리된 거래.");
                continue;
            }
            $blockHeight = null;
            $blockTime = null;
            $blockHash = null;
            if (isset($transaction['blockhash'])) {
                $blockInfo = $this->client->getBlock($transaction['blockhash']);
                $blockHeight = $blockInfo['height'];
                $blockTime = Carbon::createFromTimestamp($blockInfo['time']);
                $blockHash = $transaction['blockhash'];
            }
            $daemonTxAttributes = [
                'hash' => $transaction['txid'],
                'currency_id' =>  $currency->id,
                'receiver' => $transaction['address'],
                'amount' => abs($transaction['amount']),
                'blockhash' => $blockHash,
                'blocknumber' => $blockHeight,
                'block_time' => $blockTime,
                'status' => DaemonTxRepository::STATUS_WAIT,
                'type' => $this->getTypeBTC($transaction['category']),
                'tx_index' => $transaction['vout'],
            ];
            $procDaemonTxs[] = $daemonTxAttributes;
        }
        $this->endPoint = $txList['lastblock'];
        return $procDaemonTxs;
    }

    protected function checkDaemonTxBTC($currencyId, $hash, $txIndex, $type)
    {
        $where = ['currency_id'=>$currencyId, 'hash'=>$hash, 'tx_index'=>$txIndex, 'type'=>$type];
        $daemonTx = $this->daemonTxRepository->where($where)->get();
        return $daemonTx->isNotEmpty();
    }

    protected function getTypeBTC($type)
    {
        if ($type == self::BTC_TYPE_RECEIVE) {
            return DaemonTxRepository::TYPE_DEPOSIT;
        } else if($type == self::BTC_TYPE_SEND) {
            return DaemonTxRepository::TYPE_WITHDRAW;
        } else {
            return $type;
        }
    }

    protected function getTransactionsByBlock($symbol, $blockNumber): array
    {
        return [];
    }

    protected function getSearchCnt()
    {
        // BTC 계열은 사용하지 않음
        return ;
    }

    /**
     * 트랜잭션 검증 로직
     * @param $daemonTx
     * @throws ApplicationException
     */
    public function checkProcess($currency, $daemonTx)
    {
        $txReceipt = $this->client->getTransactionReceipt($daemonTx->hash);
        $stdConfirms = $this->getStdConfirms($daemonTx->currency_id);
        if ($stdConfirms <= $txReceipt['confirmations']) {
            Log::channel('slack_tx_info')->info("txid 확인 \n- 타입: ".$daemonTx->type." \n- txid: ".$daemonTx->hash." \n- currency: ".$daemonTx->currency->symbol." \n- amount:" . $daemonTx->amount);
            $daemonTx->status = DaemonTxRepository::STATUS_SUCCESS;
            $daemonTx->confirm = $txReceipt['confirmations'];
            if (isset($txReceipt['fee'])) {
                $daemonTx->fee = abs($txReceipt['fee']);
            }
            $daemonTx->save();
        } else if ($txReceipt['confirmations'] < 0) {
            $daemonTx->status = DaemonTxRepository::STATUS_FAIL;
            $daemonTx->confirm = $txReceipt['confirmations'];
            $daemonTx->save();
        } else {
            $this->daemonTxRepository->where(['id' => $daemonTx->id, 'status' => DaemonTxRepository::STATUS_ING])->update(['status' => DaemonTxRepository::STATUS_WAIT, 'confirm' =>$txReceipt['confirmations']]);
        }
    }

    protected function getInternalAddresses()
    {
        return [];
    }
}