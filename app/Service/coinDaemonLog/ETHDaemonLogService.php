<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 28
 * Time: 오후 8:32
 */

namespace App\Service\coinDaemonLog;

use App\Exceptions\ApplicationException;
use App\Repositories\DaemonTxRepository;
use App\Service\coinclient\ETHClient;
use App\Service\coinDeposit\CoinDepositService;
use App\Utils\StringUtils;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ETHDaemonLogService extends CoinDaemonLogAbstract
{
    protected $client;
    protected $coinSymbol = 'ETH';

    public function getBlock($blockNumber)
    {
        return $this->client->getBlock($blockNumber);
    }

    public function getBlockHeight()
    {
        return StringUtils::bcHexdec($this->client->getBlockHeight());
    }

    /**
     * @param $dsp
     * @return array
     */
    public function getTransactions($dsp, $symbol): array
    {
        $latestBlocknumber = $this->getBlockHeight();
        $latestBlocknumber = $latestBlocknumber - 10;
        $startBlock = is_null($dsp->point) ? 1 : $dsp->point;
        $endBlock = $dsp->point + $this->searchCnt;
        $endBlock = min($latestBlocknumber, $endBlock);
        $procDaemonTxs = [];
        $currency = $this->getCurrency($this->coinSymbol);
        $decimalPoint = $currency->decimal_point;

        for ($i = $startBlock; $i < $endBlock; $i++) {
            $block = $this->getBlock($i);
            $blocknumber = $i;
            $blockhash = $block['hash'];
            Log::debug("ETH 처리중 : $i");
            foreach ($block['transactions'] as $transaction) {
                try {
                    // eth 수량이 0인것들 continue
                    if (StringUtils::bcHexdec($transaction['value'])<=0) {
                        continue;
                    }

                    if ($this->checkDaemonTx($currency->id, $transaction['hash'])) {
                        Log::debug("이미 처리된 거래.");
                        continue;
                    }
                    $daemonTxAttributes = [
                        'hash' => $transaction['hash'],
                        'currency_id' =>  $currency->id,
                        'sender' => $transaction['from'],
                        'receiver' => $transaction['to'],
                        'amount' => bcdiv(StringUtils::bcHexdec($transaction['value']), bcpow('10', $decimalPoint), $decimalPoint),
                        'blockhash' => $blockhash,
                        'blocknumber' => $blocknumber,
                        'block_time' => Carbon::createFromTimestamp(hexdec($block['timestamp'])),
                        'status' => DaemonTxRepository::STATUS_WAIT,
                    ];

                    $type = $this->getType( $currency->id, $daemonTxAttributes['sender'], $daemonTxAttributes['receiver']);
                    if ($type == self::TYPE_TX_EXTERNAL) { // 거래소와 관계없는 주소일 경우 continue
                        continue;
                    } else if ($type == self::TYPE_TX_INTERNAL) { // 내부의 경우 입/출금 모두 insert
                        $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_WITHDRAW_INTERNAL;
                        $procDaemonTxs[] = $daemonTxAttributes;
                        $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_DEPOSIT_INTERNAL;
                        $procDaemonTxs[] = $daemonTxAttributes;
                    } else if($type == DaemonTxRepository::TYPE_WITHDRAW) {
                        $daemonTxAttributes['type'] = $type;
                        // 출금 데이터이므로 status_deposit 는 성공으로 update
                        $daemonTxAttributes['status_deposit'] = CoinDepositService::STATUS_DEPOSIT_SUCCESS;
                        $procDaemonTxs[] = $daemonTxAttributes;
                    } else {
                        $daemonTxAttributes['type'] = $type;
                        $procDaemonTxs[] = $daemonTxAttributes;
                    }
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    continue;
                }
            }
        }
        $this->endPoint = $endBlock;
        return $procDaemonTxs;
    }

    public function getSearchCnt()
    {
        return env('ETH_DAEMON_ONCE_SEARCH_CNT', 10);
    }

    /**
     * @param $daemonTx
     * @throws ApplicationException
     */
    public function checkProcess($currency, $daemonTx)
    {
        $txReceipt = $this->client->getTransactionReceipt($daemonTx->hash);
        $decimalPoint = $currency->decimal_point;
        $currentHeight = $this->getBlockHeight();
        $txHeight = StringUtils::bcHexdec($txReceipt['blockNumber']);
        $confirm = $currentHeight-$txHeight;

        $stdConfirms = $this->getStdConfirms($daemonTx->currency_id);
        if($txReceipt['status']=="0x1") {
            if ($stdConfirms <= $confirm) {
                Log::channel('slack_tx_info')->info("txid 확인 \n- 타입: ".$daemonTx->type." \n- txid: ".$daemonTx->hash." \n- currency: ".$daemonTx->currency->symbol." \n- amount:" . $daemonTx->amount);
                $txInfo = $this->client->getTransaction($daemonTx->hash);

                $daemonTx->status = DaemonTxRepository::STATUS_SUCCESS;
                $daemonTx->confirm = $confirm;
                $daemonTx->fee = bcmul(bcdiv(StringUtils::bcHexdec($txInfo['gasPrice']), bcpow('10', $decimalPoint), $decimalPoint),StringUtils::bcHexdec($txReceipt['gasUsed']), $decimalPoint);
                $daemonTx->save();
            } else {
                $this->daemonTxRepository->update($daemonTx->id,['status' => DaemonTxRepository::STATUS_WAIT, 'confirm'=>$confirm]);
            }
        } else {
            $daemonTx->status = DaemonTxRepository::STATUS_FAIL;
            $daemonTx->confirm = $confirm;
            $daemonTx->save();
        }
    }

    protected function getTransactionsByBlock($symbol, $blockNumber): array
    {
        $procDaemonTxs = [];
        $currency = $this->getCurrency($this->coinSymbol);
        $decimalPoint = $currency->decimal_point;

        $block = $this->getBlock($blockNumber);
        $blockhash = $block['hash'];
        Log::debug("ETH 처리중 : $blockNumber");
        foreach ($block['transactions'] as $transaction) {
            try {
                // eth 수량이 0인것들 continue
                if (StringUtils::bcHexdec($transaction['value'])<=0) {
                    continue;
                }

                if ($this->checkDaemonTx($currency->id, $transaction['hash'])) {
                    Log::debug("이미 처리된 거래.");
                    continue;
                }
                $daemonTxAttributes = [
                    'hash' => $transaction['hash'],
                    'currency_id' =>  $currency->id,
                    'sender' => $transaction['from'],
                    'receiver' => $transaction['to'],
                    'amount' => bcdiv(StringUtils::bcHexdec($transaction['value']), bcpow('10', $decimalPoint), $decimalPoint),
                    'blockhash' => $blockhash,
                    'blocknumber' => $blockNumber,
                    'block_time' => Carbon::createFromTimestamp(hexdec($block['timestamp'])),
                    'status' => DaemonTxRepository::STATUS_WAIT,
                ];

                $type = $this->getType( $currency->id, $daemonTxAttributes['sender'], $daemonTxAttributes['receiver']);
                if ($type == self::TYPE_TX_EXTERNAL) { // 거래소와 관계없는 주소일 경우 continue
                    continue;
                } else if ($type == self::TYPE_TX_INTERNAL) { // 내부의 경우 입/출금 모두 insert
                    $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_WITHDRAW_INTERNAL;
                    $procDaemonTxs[] = $daemonTxAttributes;
                    $daemonTxAttributes['type'] = DaemonTxRepository::TYPE_DEPOSIT_INTERNAL;
                    $procDaemonTxs[] = $daemonTxAttributes;
                } else if($type == DaemonTxRepository::TYPE_WITHDRAW) {
                    $daemonTxAttributes['type'] = $type;
                    // 출금 데이터이므로 status_deposit 는 성공으로 update
                    $daemonTxAttributes['status_deposit'] = DaemonTxRepository::STATUS_DEPOSIT_SUCCESS;
                    $procDaemonTxs[] = $daemonTxAttributes;
                } else {
                    $daemonTxAttributes['type'] = $type;
                    $procDaemonTxs[] = $daemonTxAttributes;
                }
            } catch (Exception $e) {
                Log::error($e->getMessage());
                continue;
            }
        }
        return $procDaemonTxs;
    }

    protected function getInternalAddresses()
    {
        return [env('ETH_OUT_ADDRESS')];
    }
}