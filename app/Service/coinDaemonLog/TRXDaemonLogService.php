<?php


namespace App\Service\coinDaemonLog;


use App\Exceptions\ApplicationException;
use Illuminate\Support\Facades\Log;

class TRXDaemonLogService extends CoinDaemonLogAbstract
{
    // TRX
    const TYPE_TRANSACTION_TRX = 'TransferContract';
    // BTT
    const TYPE_TRANSACTION_TRC10 = 'TransferAssetContract';
    // USDT, WINK
    const TYPE_TRANSACTION_TRC20 = 'TriggerSmartContract';

    protected $coinSymbol = 'TRX';

    protected function getBlockHeight()
    {
        return $this->client->getBlockHeight();
    }

    protected function getTransactions($dsp, $symbol): array
    {
        $latestBlocknumber = $this->getBlockHeight();
        $latestBlocknumber = $latestBlocknumber - 10;
        $startBlock = is_null($dsp->point) ? 1 : $dsp->point;
        $startBlock = intval($startBlock);
        $endBlock = $dsp->point + $this->searchCnt;
        $endBlock = min($latestBlocknumber, $endBlock);
        $procDaemonTxs = [];
        $currency = $this->getCurrency($this->coinSymbol);
        $decimalPoint = $currency->decimal_point;
        for ($i = $startBlock; $i < $endBlock; $i++) {
            $block = $this->client->getBlock($i);
            $blocknumber = $i;
            $blockhash = $block['blockID'];
            Log::debug("TRX 처리중 : $i");
            foreach ($block['transactions'] as $transaction) {

            }
        }
    }

    protected function getTransactionsByBlock($symbol, $blockNumber): array
    {
        // TODO: Implement getTransactionsByBlock() method.
    }

    protected function getSearchCnt()
    {
        return env('TRX_DAEMON_ONCE_SEARCH_CNT', 10);
    }

    /**
     * 트랜잭션 검증 로직
     * @param $daemonTx
     * @throws ApplicationException
     */
    public function checkProcess($currency, $daemonTx)
    {
        // TODO: Implement checkProcess() method.
    }

    protected function getInternalAddresses()
    {
        return [];
    }
}