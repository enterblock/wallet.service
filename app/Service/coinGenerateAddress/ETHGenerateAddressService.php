<?php


namespace App\Service\coinGenerateAddress;


use App\Exceptions\ApplicationException;
use App\Exceptions\DaemonException;
use App\Models\Currency;
use App\Repositories\DaemonSearchPointRepository;
use App\Repositories\GenerateAddressRepository;
use App\Repositories\GenerateAddressTransactionRepository;
use App\Service\coinclient\CoinClientFactory;
use Illuminate\Support\Facades\Log;

class ETHGenerateAddressService extends GenerateAddressAbstract
{
    function getDaemonAddrList($currency, $addrCnt)
    {
        $searchPoint = $this->daemonSearchPointRepository->where([
            'currency_id' => $currency->id,
            'type' => DaemonSearchPointRepository::TYPE_ADDR_INDEX,
        ])->first();

        $client = $this->coinClientFactory->getClient($currency);
        $managerAddress = env($currency->symbol . "_MANAGER_ADDRESS");
        $usersLength = $client->getUsersLength($managerAddress);

        $inserAddrArr = [];
        if ($searchPoint->point < $usersLength) { // 생성된 주소 index < 블록체인에 생성된 length
            $diff = $usersLength - $searchPoint->point;
            $maxGetUserLength = env($currency->symbol . "_MAX_GET_USER_LENGTH", 50);
            $startPoint = $searchPoint->point;
            $endPoint = $searchPoint->point + min($diff, $maxGetUserLength);
            // 데몬 생성 주소 조회
            $addrData = $client->getUsers($managerAddress, $startPoint, $endPoint);
            // 중복체크
            $duplicateAddr = $this->generateAddressRepository->whereIn('address', $addrData)->where('currency_id', $currency->id)->pluck("address");
            $duplicateAddrArr = $duplicateAddr->toArray();
            $inserAddrArr = array_diff($addrData, $duplicateAddrArr);
            // 조회 인덱스 업데이트
            $this->daemonSearchPointRepository->update($searchPoint->id, [
                'point' => $endPoint
            ]);
        } else { // 생성된 주소 index >= 블록체인에 생성된 length
            // => DB에 주소생성 트랜잭션이 V상태인것이 있는지 확인 (있으면 exit)
            // tpurcow_generate_address_transaction
            $verificateCnt = $this->generateAddressTransactionRepository->where(['currency_info_id'=>$currency->id,'status'=>GenerateAddressTransactionRepository::STATUS_WAIT_VERIFICATE])->count();
            if ($verificateCnt > 0) { // pendding 중인 트랜잭션이 있음
                return [];
            }

            // => eth_call - newUser(uint256)
            $hash = $client->sendNewUsers($currency, env($currency->symbol . '_MAX_CREATE_USER_LENGTH', 20));
            // => insert 주소생성 트랜잭션 txid 상태 V
            $this->generateAddressTransactionRepository->create([
                'currency_info_id' => $currency->id,
                'hash' => $hash,
                'status' => GenerateAddressTransactionRepository::STATUS_WAIT_VERIFICATE,
            ]);
        }

        return $inserAddrArr;
    }

    function checkProcess($currency)
    {
        $client = $this->coinClientFactory->getClient($currency);
        $verificateGenTxArr = $this->generateAddressTransactionRepository->where(['currency_info_id'=>$currency->id,'status'=>GenerateAddressTransactionRepository::STATUS_WAIT_VERIFICATE])->get();
        foreach ($verificateGenTxArr as $verificateGenTx) {
            try {
                $receipt = $client->getTransactionReceipt($verificateGenTx->hash);
                $client->checkReceipt($receipt);
                $verificateGenTx->status = GenerateAddressTransactionRepository::STATUS_SUCCESS;
                Log::info("트랜잭션 검증 성공 : $verificateGenTx->hash");
                $verificateGenTx->save();
            } catch (DaemonException $e) {
                if($e->getCode()==DaemonException::RECEIPT_ERR){
                    Log::error($e);
                    continue;
                }
            }
        }
    }
}