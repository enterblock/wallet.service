<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 28
 * Time: 오후 8:32
 */

namespace App\Service\coinGenerateAddress;


use App\Exceptions\ApplicationException;


class GenerateAddressFactory
{
    /**
     * @param $symbol
     * @return GenerateAddressAbstract
     * @throws ApplicationException
     */
    public function build($symbol): GenerateAddressAbstract
    {
        $client = $symbol."GenerateAddressService";
        if (!app()->bound($client)) {
            throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
        }
        return resolve($client);
    }
}