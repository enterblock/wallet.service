<?php


namespace App\Service\coinGenerateAddress;


use App\Models\CurrencyInfo;
use App\Repositories\DaemonSearchPointRepository;
use App\Repositories\GenerateAddressRepository;
use App\Repositories\GenerateAddressTransactionRepository;
use App\Service\coinclient\CoinClientFactory;
use App\Service\CurrencyService;
use Illuminate\Support\Facades\Log;

abstract class GenerateAddressAbstract
{
    /**
     * @var GenerateAddressRepository
     */
    protected $generateAddressRepository;
    /**
     * @var CoinClientFactory
     */
    protected $coinClientFactory;
    /**
     * @var CurrencyService
     */
    protected $currencyService;
    /**
     * @var DaemonSearchPointRepository
     */
    protected $daemonSearchPointRepository;
    /**
     * @var GenerateAddressTransactionRepository
     */
    protected $generateAddressTransactionRepository;

    public function __construct(GenerateAddressRepository $generateAddressRepository, CoinClientFactory $coinClientFactory, CurrencyService $currencyService,
                                DaemonSearchPointRepository $daemonSearchPointRepository, GenerateAddressTransactionRepository $generateAddressTransactionRepository)
    {
        $this->generateAddressRepository = $generateAddressRepository;
        $this->coinClientFactory = $coinClientFactory;
        $this->currencyService = $currencyService;
        $this->daemonSearchPointRepository = $daemonSearchPointRepository;
        $this->generateAddressTransactionRepository = $generateAddressTransactionRepository;
    }

    abstract function getDaemonAddrList($currency, $addrCnt);

    /**
     * @param CurrencyInfo $currency
     * @param $addrCnt
     */
    public function generateAddress(CurrencyInfo $currency, $addrCnt)
    {
        $addrArr = $this->getDaemonAddrList($currency, $addrCnt);
        $addrArrCnt = count($addrArr);
        if ($addrArrCnt > 0) {
            $genAddr = [];
            foreach ($addrArr as $addr) {
                $genAddr[] = [
                    'currency_id' => $currency->id,
                    'address' => $addr,
                ];
            }
            Log::info("$currency->symbol 주소 할당 대기 테이블(tpurcow_generate_address) insert cnt : $addrArrCnt");
            $this->generateAddressRepository->insert($genAddr);
        }
    }

    abstract function checkProcess($currency);
}