<?php


namespace App\Service\coinGenerateAddress;


use App\Exceptions\ApplicationException;
use App\Models\Currency;
use App\Repositories\GenerateAddressRepository;
use App\Service\coinclient\CoinClientFactory;

class BTCGenerateAddressService extends GenerateAddressAbstract
{
    function getDaemonAddrList($currency, $addrCnt)
    {
        $client = $this->coinClientFactory->getClient($currency);
        $addrArr = [];
        for ($i = 0; $i < $addrCnt; $i++) {
            $addrArr[] = $client->createCoinAddr();
        }

        return $addrArr;
    }

    function checkProcess($currency)
    {
        return;
    }
}