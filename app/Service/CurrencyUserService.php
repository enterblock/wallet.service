<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 9. 27
 * Time: 오전 10:52
 */

namespace App\Service;


use App\Exceptions\ApplicationException;
use App\Models\CurrencyUser;
use App\Repositories\CurrencyInfoRepository;
use App\Repositories\CurrencyUserRepository;
use App\Repositories\MemberRepository;
use App\Repositories\UserAssetsLogRepository;
use Illuminate\Support\Facades\Log;

class CurrencyUserService
{
    private $currencyUserRepo;
    /**
     * @var MemberRepository
     */
    private $memberRepository;
    /**
     * @var UserAssetsLogRepository
     */
    private $userAssetsLogRepository;
    /**
     * @var CurrencyInfoRepository
     */
    private $currencyInfoRepository;

    public function __construct(CurrencyUserRepository $currencyUserRepo, MemberRepository $memberRepository, UserAssetsLogRepository $userAssetsLogRepository,
                                CurrencyInfoRepository $currencyInfoRepository)
    {
        $this->currencyUserRepo = $currencyUserRepo;
        $this->memberRepository = $memberRepository;
        $this->userAssetsLogRepository = $userAssetsLogRepository;
        $this->currencyInfoRepository = $currencyInfoRepository;
    }

    public function findByAddr($currencyId, $addr)
    {
        return $this->currencyUserRepo->where(['currency_id'=>$currencyId, 'chongzhi_url'=>$addr])->first();
    }

    public function getCurrencyUser($currencyId, $memberId)
    {
        return $this->currencyUserRepo->where(['currency_id'=>$currencyId, 'member_id'=>$memberId])->first();
    }

    /**
     * @param $currencyId
     * @param $memberId
     * @return CurrencyUser
     */
    public function createCurrencyUser($currencyId, $memberId)
    {
        return $this->currencyUserRepo->createCurrencyUser($currencyId, $memberId);
    }

    public function getTokenCurrencyUser($tokenCurrencyIds, $member_id)
    {
        return $this->currencyUserRepo->whereIn('currency_id', $tokenCurrencyIds)->where('member_id',$member_id)->where('chongzhi_url', '<>', '')->get();
    }

    public function getUserAddressByCurrencyAddress($currencyId, $address, $tag=null)
    {
        $where = [
            'currency_id'=>$currencyId,
            'chongzhi_url' => $address,
        ];
        if (!is_null($tag) && $this->currencyInfoRepository->where(['id'=>$currencyId,'address_type'=>CurrencyInfoRepository::ADDRESS_TYPE_SUB_ADDRESS])->get()->isNotEmpty()) {
            $member = $this->memberRepository->where(['show_uid' => $tag])->get();
            if (count($member) <= 0 || count($member) > 1) {
                return null;
            }
            $where['member_id'] = $member->first()->member_id;
        }
        return $this->currencyUserRepo->where($where)->first();
    }
}