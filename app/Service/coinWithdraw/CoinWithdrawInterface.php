<?php


namespace App\Service\coinWithdraw;


use App\Exceptions\DaemonException;

interface CoinWithdrawInterface
{
    /**
     * @param $currency
     * @param $attribute
     * @return mixed
     * @throws DaemonException
     */
    public function send($currency, $attribute);
    /**
     * @param $currency
     * @param $txid
     * @return mixed
     * @throws DaemonException
     */
    public function checkTransaction($currency, $txid);
}