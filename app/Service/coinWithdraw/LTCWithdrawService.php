<?php


namespace App\Service\coinWithdraw;


use App\Exceptions\DaemonException;
use App\Service\coinclient\CoinClientFactory;

class LTCWithdrawService implements CoinWithdrawInterface
{
    /**
     * @var CoinClientFactory
     */
    protected $coinClientFactory;

    public function __construct(CoinClientFactory $coinClientFactory)
    {
        $this->coinClientFactory = $coinClientFactory;
    }

    /**
     * 트랜잭션 send
     *
     * @param $currency
     * $attribute['receiver']
     * $attribute['receiver_sub']
     * $attribute['amount']
     * @param array $attribute (See above)
     * @return mixed
     * @throws DaemonException
     * @throws \App\Exceptions\ApplicationException
     */
    public function send($currency, $attribute)
    {
        $client = $this->coinClientFactory->getClient($currency);
        $sendParams = [$attribute['receiver'], floatval($attribute['amount'])];
        $txid = $client->send($currency, $sendParams);
        if (is_null($txid)) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR);
        }
        return $txid;
    }

    /**
     * 트랜잭션 유효성 체크
     *
     * @param $currency
     * @param $txid
     * @return bool
     * @throws \App\Exceptions\ApplicationException
     */
    public function checkTransaction($currency, $txid)
    {
        $client = $this->coinClientFactory->getClient($currency);
        $txReceipt = $client->getTransactionReceipt($txid);
        if(!is_null($txReceipt)) {
            return true;
        }
        return false;
    }
}