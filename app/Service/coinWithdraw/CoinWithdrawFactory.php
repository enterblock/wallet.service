<?php
/**
 * Created by PhpStorm.
 * User: dhseo
 * Date: 19. 6. 28
 * Time: 오후 8:32
 */

namespace App\Service\coinWithdraw;


use App\Exceptions\ApplicationException;
use App\Service\CurrencyService;


class CoinWithdrawFactory
{
    /**
     * @var CurrencyService
     */
    private $currencyService;

    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    /**
     * @param $symbol
     * @return CoinWithdrawInterface
     * @throws ApplicationException
     */
    public function build($symbol): CoinWithdrawInterface
    {
        $client = $symbol."WithdrawService";
        if (app()->bound($client)) {
            return resolve($client);
        } else {
            // symbol 확인
            $currency = $this->currencyService->findBySymbol($symbol);
            if (is_null($currency) || is_null($currency->parent)) {
                throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
            }
            $client = $currency->parent->symbol."TokenWithdrawService";
            if (app()->bound($client)) {
                return resolve($client);
            }
        }
    }
}