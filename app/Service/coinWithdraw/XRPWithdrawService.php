<?php


namespace App\Service\coinWithdraw;


use App\Exceptions\DaemonException;
use App\Service\coinclient\CoinClientFactory;
use App\Service\coinDaemonLog\XRPDaemonLogService;

class XRPWithdrawService implements CoinWithdrawInterface
{
    /**
     * @var CoinClientFactory
     */
    protected $coinClientFactory;

    public function __construct(CoinClientFactory $coinClientFactory)
    {
        $this->coinClientFactory = $coinClientFactory;
    }

    /**
     * 트랜잭션 send
     *
     * @param $currency
     * $attribute['receiver']
     * $attribute['receiver_sub']
     * $attribute['amount']
     * @param array $attribute (See above)
     * @return mixed
     * @throws DaemonException
     * @throws \App\Exceptions\ApplicationException
     */
    public function send($currency, $attribute)
    {
        $client = $this->coinClientFactory->getClient($currency);
        $txid = $client->send($currency, $attribute);
        if (is_null($txid)) {
            throw new DaemonException(DaemonException::COIN_SEND_ERR);
        }
        return $txid;
    }

    /**
     * 트랜잭션 유효성 체크
     *
     * @param $currency
     * @param $txid
     * @return bool
     * @throws \App\Exceptions\ApplicationException
     */
    public function checkTransaction($currency, $txid)
    {
        $client = $this->coinClientFactory->getClient($currency);
        $txReceipt = $client->getTransactionReceipt($txid);
        $txResultPrefix = substr($txReceipt['meta']['TransactionResult'], 0, 3);
        if(in_array($txResultPrefix, XRPDaemonLogService::XRP_TRANSACTION_RESULT_SUCCESS_CODE)) {
            return true;
        }
        return false;
    }
}