<?php


namespace App\Service\coinWithdraw;


use App\Exceptions\ApplicationException;
use App\Exceptions\DaemonException;
use App\Repositories\CoinTransactionRepository;
use App\Repositories\UsedAssetsRepository;
use App\Repositories\UserAssetsLogRepository;
use App\Service\coinclient\CoinClientFactory;
use App\Service\common\UserAssetsService;
use App\Service\CurrencyService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CoinWithdrawService
{
    /**
     * @var CoinTransactionRepository
     */
    private $coinTransactionRepository;
    /**
     * @var CurrencyService
     */
    private $currencyService;
    /**
     * @var UserAssetsService
     */
    private $userAssetsService;
    /**
     * @var CoinClientFactory
     */
    private $coinClientFactory;
    /**
     * @var CoinWithdrawFactory
     */
    private $coinWithdrawFactory;

    public function __construct(CoinTransactionRepository $coinTransactionRepository, CurrencyService $currencyService, UserAssetsService $userAssetsService,
                                CoinClientFactory $coinClientFactory, CoinWithdrawFactory $coinWithdrawFactory)
    {
        $this->coinTransactionRepository = $coinTransactionRepository;
        $this->currencyService = $currencyService;
        $this->userAssetsService = $userAssetsService;
        $this->coinClientFactory = $coinClientFactory;
        $this->coinWithdrawFactory = $coinWithdrawFactory;
    }

    /**
     * @param $coinSymbol
     * @throws ApplicationException
     */
    public function process($coinSymbol)
    {
        $currencyId = $this->currencyService->getCurrencyIdBySymbol($coinSymbol);
        $waitWithdrawList = $this->getWaitWithdrawList($currencyId);
        foreach ($waitWithdrawList as $waitWithdrawCoinTransaction) {
            try {
                // 코인 출금 상태 확인
                if (!$this->currencyService->checkEnableWithdraw($waitWithdrawCoinTransaction->currency_id)) {
                    throw new ApplicationException(ApplicationException::INACTIVE_WITHDRAW); // 출금 비활성 상태
                }
                // 출금 진행
                $currency = $this->currencyService->getCurrency($waitWithdrawCoinTransaction->currency_id);
                $coinWithdrawService = $this->coinWithdrawFactory->build($currency->symbol);
                $hash = $coinWithdrawService->send($currency, [
                    'receiver'=>$waitWithdrawCoinTransaction->receiver,
                    'receiver_sub'=>$waitWithdrawCoinTransaction->receiver_sub,
                    'amount'=>$waitWithdrawCoinTransaction->amount,
                ]);
                $waitWithdrawCoinTransaction->hash = $hash;
                $waitWithdrawCoinTransaction->status = CoinTransactionRepository::STATUS_WAIT_VERIFICATE;
                $waitWithdrawCoinTransaction->save();
            } catch (ApplicationException $e) {
                Log::emergency('출금처리 APP error : ' . $e->getMessage());
                if ($e->getCode()!=ApplicationException::INACTIVE_WITHDRAW) {
                    $waitWithdrawCoinTransaction->status = CoinTransactionRepository::STATUS_FAIL;
                    $waitWithdrawCoinTransaction->save();
                } else {
                    $waitWithdrawCoinTransaction->status = CoinTransactionRepository::STATUS_WAIT;
                    $waitWithdrawCoinTransaction->save();
                }
                continue;
            } catch (DaemonException $e) {
                Log::emergency('출금처리 Daemon error : ' . $e->getMessage());
                if ($e->getCode()!=DaemonException::COIN_SEND_ERR) {
                    $waitWithdrawCoinTransaction->status = CoinTransactionRepository::STATUS_FAIL;
                    $waitWithdrawCoinTransaction->save();
                } else {
                    $waitWithdrawCoinTransaction->status = CoinTransactionRepository::STATUS_WAIT;
                    $waitWithdrawCoinTransaction->save();
                }
                continue;
            } catch (Exception $e) {
                Log::emergency('출금처리 error : ' . $e->getMessage());
                $waitWithdrawCoinTransaction->status = CoinTransactionRepository::STATUS_FAIL;
                $waitWithdrawCoinTransaction->save();
                continue;
            }
        }
    }

    public function checkProcess($coinSymbol)
    {
        $currencyId = $this->currencyService->getCurrencyIdBySymbol($coinSymbol);
        $verificateWithdrawList = $this->getVerificateWithdrawList($currencyId);
        foreach ($verificateWithdrawList as $coinTransaction) {
            try {
                // 코인 출금 상태 확인
                if (!$this->currencyService->checkEnableWithdraw($coinTransaction->currency_id)) {
                    throw new ApplicationException(ApplicationException::INACTIVE_WITHDRAW); // 출금 비활성 상태
                }
                // 트랜잭션 검증
                $currency = $this->currencyService->getCurrency($coinTransaction->currency_id);
                $coinWithdrawService = $this->coinWithdrawFactory->build($currency->symbol);
                if (!$coinWithdrawService->checkTransaction($currency, $coinTransaction->hash)) {
                    $coinTransaction->status = CoinTransactionRepository::STATUS_WAIT_VERIFICATE;
                    $coinTransaction->save();
                    continue;
                }
                DB::beginTransaction();
                $coinTransaction->status = CoinTransactionRepository::STATUS_SUCCESS;
                $coinTransaction->save();
                // 회원자산 차감 & 회원자산 로그
                $this->userAssetsService->subCurrencyUserAsset(UserAssetsLogRepository::TYPE_WITHDRAW, $coinTransaction->currency_id,
                    $coinTransaction->member_id, bcadd($coinTransaction->amount, $coinTransaction->fee, env('DB_DECIMAL')), $coinTransaction->id);
                // 회원 증거금 삭제
                $this->userAssetsService->deleteUsedAssets(UsedAssetsRepository::TYPE_WITHDRAW, $coinTransaction->id);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                Log::emergency('출금 자산 처리 error : ' . $e->getMessage());
                continue;
            }
        }
    }

    public function getVerificateWithdrawList($currencyId=null)
    {
        $where = [
            'type' => CoinTransactionRepository::TYPE_WITHDRAW,
            'status' => CoinTransactionRepository::STATUS_WAIT_VERIFICATE,
        ];
        if (!is_null($currencyId)) {
            $where['currency_id'] = $currencyId;
        }
        DB::beginTransaction();
        $withdrawVerificateArr = $this->coinTransactionRepository->where($where)->lockForUpdate()->get();
        foreach ($withdrawVerificateArr as $withdrawVerificate) {
            $withdrawVerificate->status = CoinTransactionRepository::STATUS_ING;
            $withdrawVerificate->save();
        }
        DB::commit();
        return $withdrawVerificateArr;
    }

    public function getWaitWithdrawList($currencyId=null)
    {
        $where = [
            'type' => CoinTransactionRepository::TYPE_WITHDRAW,
            'status' => CoinTransactionRepository::STATUS_APPROVE,
        ];
        if (!is_null($currencyId)) {
            $where['currency_id'] = $currencyId;
        }
        DB::beginTransaction();
        $withdrawWaitArr = $this->coinTransactionRepository->where($where)->lockForUpdate()->get();
        foreach ($withdrawWaitArr as $withdrawWait) {
            $withdrawWait->status = CoinTransactionRepository::STATUS_ING;
            $withdrawWait->save();
        }
        DB::commit();
        return $withdrawWaitArr;
    }
}