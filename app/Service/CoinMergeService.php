<?php


namespace App\Service;


use App\Exceptions\ApplicationException;
use App\Exceptions\DaemonException;
use App\Models\CoinMerge;
use App\Models\CurrencyUser;
use App\Repositories\CoinMergeRepository;
use App\Repositories\CoinTransactionRepository;
use App\Repositories\CurrencyCtRepository;
use App\Repositories\CurrencyInfoRepository;
use App\Repositories\UsedAssetsRepository;
use App\Repositories\UserAssetsLogRepository;
use App\Service\coinclient\CoinClientFactory;
use App\Service\coinclient\CoinClientInterface;
use App\Service\coinWithdraw\CoinWithdrawFactory;
use App\Service\common\UserAssetsService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CoinMergeService
{

    /**
     * @var CoinMergeRepository
     */
    private $coinMergeRepository;
    /**
     * @var CurrencyService
     */
    private $currencyService;
    /**
     * @var CurrencyInfoRepository
     */
    private $currencyInfoRepository;
    /**
     * @var CoinClientFactory
     */
    private $coinClientFactory;

    /**
     * CoinMergeService constructor.
     * @param CoinMergeRepository $coinMergeRepository
     * @param CurrencyService $currencyService
     * @param CurrencyInfoRepository $currencyInfoRepository
     * @param CoinClientFactory $coinClientFactory
     */
    public function __construct(CoinMergeRepository $coinMergeRepository, CurrencyService $currencyService,
                                CurrencyInfoRepository $currencyInfoRepository, CoinClientFactory $coinClientFactory)
    {
        $this->coinMergeRepository = $coinMergeRepository;
        $this->currencyService = $currencyService;
        $this->currencyInfoRepository = $currencyInfoRepository;
        $this->coinClientFactory = $coinClientFactory;
    }

    public function process($coinSymbol=null)
    {
        $currencyId = null;
        if (!is_null($coinSymbol)) {
            $currency = $this->currencyService->findBySymbol($coinSymbol);
            if (is_null($currency) || $currency->merge_active != CurrencyInfoRepository::MERGE_ACTIVE) {
                throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
            }
            $currencyId = $currency->id;
        }
        $coinMergeList = $this->getCoinMergeList($currencyId);
        foreach ($coinMergeList as $coinMerge) {
            try {
                $currency = $this->currencyService->getCurrency($coinMerge->currency_info_id);
                $client = $this->coinClientFactory->getClient($currency);

                $balance = $client->balance($currency, $coinMerge->address);

                $mergeParams = [
                    'sender' => $coinMerge->address,
                    'amount' => $coinMerge->amount,
                ];

                if (bccomp($coinMerge->amount, $balance, $currency->decimal_point) > 0) { // $coinMerge->amount > $balance
                    $mergeParams['amount'] = $balance;
                }

                $sendHash = $client->merge($currency, $mergeParams);
                $coinMerge->hash = $sendHash;
                $coinMerge->send_amount = $mergeParams['amount'];
                $coinMerge->status = CoinMergeRepository::STATUS_VERIFICATE;
                $coinMerge->save();
            } catch (Exception $e) {
                $coinMerge->status = CoinMergeRepository::STATUS_FAIL;
                $coinMerge->save();
                Log::error($e);
                continue;
            }
        }
    }

    public function checkProcess($coinSymbol)
    {
        $currencyId = null;
        if (!is_null($coinSymbol)) {
            $currency = $this->currencyService->findBySymbol($coinSymbol);
            if (is_null($currency) || $currency->merge_active != CurrencyInfoRepository::MERGE_ACTIVE) {
                throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
            }
            $currencyId = $currency->id;
        }
        $coinMergeVerificateList = $this->getCoinMergeVerificateList($currencyId);
        foreach ($coinMergeVerificateList as $coinMerge) {
            try {
                $currency = $this->currencyService->getCurrency($coinMerge->currency_info_id);
                $client = $this->coinClientFactory->getClient($currency);

                $txReceipt = $client->getTransactionReceipt($coinMerge->hash);

                $client->checkReceipt($txReceipt);
                $coinMerge->status = CoinMergeRepository::STATUS_SUCCESS;
                $coinMerge->save();
            } catch (Exception $e) {
                if ($e->getCode() == DaemonException::RECEIPT_ERR) {
                    $coinMerge->status = CoinMergeRepository::STATUS_VERIFICATE;
                } else {
                    $coinMerge->status = CoinMergeRepository::STATUS_FAIL;
                    Log::error($e);
                }
                $coinMerge->save();
                continue;
            }
        }
    }

    /**
     * @param $currencyId
     * @return mixed
     */
    private function getCoinMergeList($currencyId=null)
    {
        $query = $this->coinMergeRepository->where([
            'status' => CoinMergeRepository::STATUS_WAIT,
        ]);
        if (!is_null($currencyId)) {
            $query->where('currency_info_id', $currencyId);
        }

        DB::beginTransaction();
        $coinMergeList = $query->lockForUpdate()->get();
        foreach ($coinMergeList as $coinMerge) {
            $coinMerge->status = CoinMergeRepository::STATUS_ING;
            $coinMerge->save();
        }
        DB::commit();

        return $coinMergeList;
    }

    private function getCoinMergeVerificateList($currencyId)
    {
        $query = $this->coinMergeRepository->where([
            'status' => CoinMergeRepository::STATUS_VERIFICATE,
        ]);
        if (!is_null($currencyId)) {
            $query->where('currency_info_id', $currencyId);
        }

        DB::beginTransaction();
        $coinMergeList = $query->lockForUpdate()->get();
        foreach ($coinMergeList as $coinMerge) {
            $coinMerge->status = CoinMergeRepository::STATUS_ING;
            $coinMerge->save();
        }
        DB::commit();

        return $coinMergeList;
    }
}