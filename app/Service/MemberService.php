<?php


namespace App\Service;


use App\Repositories\MemberRepository;

class MemberService
{
    /**
     * @var MemberRepository
     */
    private $memberRepository;

    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    public function isMember($id)
    {
        return !is_null($this->memberRepository->find($id));
    }
    public function getMember($id)
    {
        return $this->memberRepository->find($id);
    }

}