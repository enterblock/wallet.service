<?php


namespace App\Service;


use App\Exceptions\ApplicationException;
use App\Models\CurrencyUser;
use App\Repositories\CoinTransactionRepository;
use App\Repositories\CurrencyCtRepository;
use App\Repositories\CurrencyInfoRepository;
use App\Repositories\GenerateAddressRepository;
use App\Repositories\UsedAssetsRepository;
use App\Repositories\UserAssetsLogRepository;
use App\Service\coinclient\CoinClientFactory;
use App\Service\coinclient\CoinClientInterface;
use App\Service\coinWithdraw\CoinWithdrawFactory;
use App\Service\common\UserAssetsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CoinService
{
    /**
     * @var CurrencyUserService
     */
    private $currencyUserService;
    /**
     * @var GenerateAddressService
     */
    private $generateAddressService;

    /**
     * @var CurrencyCtRepository
     */
    private $currencyCtRepository;
    /**
     * @var CoinTransactionRepository
     */
    private $coinTransactionRepository;
    /**
     * @var userAssetsService
     */
    private $userAssetsService;
    /**
     * @var CurrencyInfoRepository
     */
    private $currencyInfoRepository;
    /**
     * @var CoinClientFactory
     */
    private $coinClientFactory;
    /**
     * @var CoinWithdrawFactory
     */
    private $coinWithdrawFactory;
    /**
     * @var GenerateAddressRepository
     */
    private $generateAddressRepository;

    public function __construct(CurrencyUserService $currencyUserService, GenerateAddressRepository $generateAddressRepository
        , CurrencyCtRepository $currencyCtRepository, CoinTransactionRepository $coinTransactionRepository, UserAssetsService $userAssetsService
        , CurrencyInfoRepository $currencyInfoRepository, CoinClientFactory $coinClientFactory, CoinWithdrawFactory $coinWithdrawFactory)
    {
        $this->currencyUserService = $currencyUserService;
        $this->currencyCtRepository = $currencyCtRepository;
        $this->coinTransactionRepository = $coinTransactionRepository;
        $this->userAssetsService = $userAssetsService;
        $this->currencyInfoRepository = $currencyInfoRepository;
        $this->coinClientFactory = $coinClientFactory;
        $this->coinWithdrawFactory = $coinWithdrawFactory;
        $this->generateAddressRepository = $generateAddressRepository;
    }

    /**
     * @param $currency
     * @return mixed
     * @throws ApplicationException
     */
    public function createCoinAddr($currency, $member)
    {
        try {
            // 이미 생성된 회원 코인 주소 있는지 검증.
            $currencyUser = $this->currencyUserService->getCurrencyUser($currency->id, $member->member_id);
            // 없으면 row 생성
            if (is_null($currencyUser)) {
                $currencyUser = $this->currencyUserService->createCurrencyUser($currency->id, $member->member_id);
            }

            if (empty($currencyUser->chongzhi_url)) {
                if ($this->isSubAddressType($currency->id)) {
                    if (empty(env($currency->symbol . "_ADDRESS"))) {
                        throw new ApplicationException(ApplicationException::CHECK_COIN_ENV);
                    }
                    $currencyUser->chongzhi_url = env($currency->symbol . "_ADDRESS");
                    $currencyUser->save();
                } else if(!is_null($currency->parent)) {
                    // 자식들 코인 id 조회.
                    $tokenCurrencyIds = $currency->parent->children->pluck('id')->toArray();
                    $tokenCurrencyIds[] = $currency->parent->id;
                    $tokenCurrencyUser = $this->currencyUserService->getTokenCurrencyUser($tokenCurrencyIds, $member->member_id);
                    if (count($tokenCurrencyUser) > 0) {
                        $currencyUser->chongzhi_url = $tokenCurrencyUser->first()->chongzhi_url;
                        $currencyUser->save();
                    } else {
                        // 없으면 parent 코인 주소 조회하여 할당.
                        $this->updateAddress($currency->parent->id, $currencyUser);
                    }
                } else {
                    $this->updateAddress($currency->id, $currencyUser);
                }

                return $currencyUser->chongzhi_url;
            } else {
                return $currencyUser->chongzhi_url;
            }
        } catch (ApplicationException $e) {
            throw $e;
        }
    }

    public function daemonCreateCoinAddr($currency)
    {
        try {
            $client = $this->coinClientFactory->getClient($currency);
            return $client->createCoinAddr();
        } catch (ApplicationException $e) {
            throw $e;
        }
    }

    public function withdraw($currency, $attributes)
    {
        try {
            $coinWithdrawService = $this->coinWithdrawFactory->build($currency->symbol);
            return $coinWithdrawService->send($currency, $attributes);
        } catch (ApplicationException $e) {
            throw $e;
        }
    }

    public function merge($currency, $attributes)
    {
        try {
            $client = $this->coinClientFactory->getClient($currency);
            return $client->merge($currency, $attributes);
        } catch (ApplicationException $e) {
            throw $e;
        }
    }

    public function balance($currency, $addr)
    {
        try {
            $client = $this->coinClientFactory->getClient($currency);
            return $client->balance($currency, $addr);
        } catch (ApplicationException $e) {
            throw $e;
        }
    }

    /**
     * @param $createCurrencyId
     * @param CurrencyUser $currencyUser
     * @throws ApplicationException
     */
    private function updateAddress($createCurrencyId, CurrencyUser $currencyUser): void
    {
        // 주소 생성테이블 W상태 조회
        $waitAddress = $this->getWaitAddress($createCurrencyId);
        if (is_null($waitAddress)) {
            throw new ApplicationException(ApplicationException::NOT_GENERATE_ADDRESS,
                '생성주소 없음. 코인 ID: ' . $createCurrencyId); // 생성된 주소 없음
        }
        // 조회된 주소 상태 I상태로 변경
        if (!$this->setStatusIng($waitAddress)) {
            throw new ApplicationException(ApplicationException::UPDATE_FAIL); // DB 업데이트 실패
        }
        // 트랜잭션
        DB::transaction(function () use ($waitAddress, $currencyUser) {
            // 회원 코인 주소 update
            $currencyUser->chongzhi_url = $waitAddress->address;
            $currencyUser->save();
            if (!$this->setStatusSuccess($waitAddress, $currencyUser)) {
                throw new ApplicationException(ApplicationException::UPDATE_FAIL); // DB 업데이트 실패
            }
        });
    }

    private function getWaitAddress($currencyId)
    {
        return $this->generateAddressRepository->where(['currency_id'=>$currencyId,'status' => GenerateAddressRepository::STATUS_WAIT])->first();
    }

    private function setStatusIng($generateAddress)
    {
        $generateAddress->status = GenerateAddressRepository::STATUS_ING;
        return $generateAddress->save();
    }

    private function setStatusSuccess($generateAddress, $currencyUser)
    {
        $genAddress = $this->generateAddressRepository->where(['id'=>$generateAddress->id, 'status'=>GenerateAddressRepository::STATUS_ING])->get()->first();
        $genAddress->status = GenerateAddressRepository::STATUS_SUCCESS;
        $genAddress->member_id = $currencyUser->member_id;
        return $genAddress->save();
    }

    public function checkWaitAddressCnt($currency_id)
    {
        return $this->generateAddressRepository->where(['currency_id'=>$currency_id, 'status'=>GenerateAddressRepository::STATUS_WAIT])->count();
    }

    /**
     * 출금 등록 처리 (내부의 경우 내부 입금도 처리함)
     *
     * @param $currency
     * @param $params
     * @throws ApplicationException
     */
    public function createWithdraw($currency, $params)
    {
        // 코인 설정 정보 조회
        $currencyCt = $this->currencyCtRepository->where(['currency_id'=>$currency->id])->first();
        // TODO 코인 하루 출금 한도 체크 => 기획 확정되면 개발.
        // 출금 inserval 체크
        $coinTransaction = $this->latestWithdraw($currency->id, $params['member_id']);
        if (!is_null($coinTransaction)) {
            $diffTimeNowAndWithdraw = $coinTransaction->created_at->diffInSeconds(Carbon::now());
            if ($diffTimeNowAndWithdraw < 60 && bccomp($coinTransaction->amount, $params['amount'], env('DB_DECIMAL'))==0) {
                throw new ApplicationException(ApplicationException::WITHDRAW_INTERVAL);
            }
        }

        $receiverCurrencyUser = null;
        if (!isset($params['receiver_sub'])) {
            $params['receiver_sub'] = null;
        }
        $receiverCurrencyUser = $this->currencyUserService->getUserAddressByCurrencyAddress($currency->id, $params['receiver'], $params['receiver_sub']);
        // 내부회원 지갑 출금인지 체크
        if (is_null($receiverCurrencyUser)) {
            $this->createWithdrawExternal($currency, $params, $currencyCt);
        } else {
            $params['receiverCurrencyUser'] = $receiverCurrencyUser;
            $this->createWithdrawInternal($currency, $params, $currencyCt);
        }
    }

    public function latestWithdraw($currencyId, $memberId)
    {
        return $this->coinTransactionRepository->where(['type'=>CoinTransactionRepository::TYPE_WITHDRAW, 'member_id'=>$memberId, 'currency_id'=>$currencyId])->orderBy('id','desc')->first();
    }

    /**
     * @param $currency
     * @param $params
     * @param $currencyCt
     * @throws ApplicationException
     */
    private function createWithdrawExternal($currency, $params, $currencyCt): void
    {
        // 회원 자산 검증
        // 사용중인 assets
        $availableAssets = $this->userAssetsService->getAvailableUserAsset($params['member_id'], $currency->id, $currency->decimal_point);
        if (bccomp($availableAssets, $params['amount'], env('DB_DECIMAL')) <= 0) {
            throw new ApplicationException(ApplicationException::LOW_BALANCE);
        }
        DB::beginTransaction();
        $insertParams = [
            'member_id' => $params['member_id'],
            'type' => CoinTransactionRepository::TYPE_WITHDRAW,
            'currency_id' => $currency->id,
            'receiver' => $params['receiver'],
            'amount' => $params['amount'],
            'fee' => $currencyCt['withdraw_fee'],
        ];
        if (isset($params['receiver_sub'])) {
            $insertParams['receiver_sub'] = $params['receiver_sub'];
        }
        // 출금 등록
        $coinTransaction = $this->coinTransactionRepository->create($insertParams);
        // 증거금 등록
        $this->userAssetsService->createUsedAssets([
            'type' => UsedAssetsRepository::TYPE_WITHDRAW,
            'member_id' => $params['member_id'],
            'currency_id' => $currency->id,
            'amount' => bcadd($params['amount'],$currencyCt['withdraw_fee'],$currency->decimal_point),
            'ref_id' => $coinTransaction->id,
        ]);
        DB::commit();
    }

    /**
     * @param $currency
     * @param $params
     * @param $currencyCt
     * @throws ApplicationException
     */
    private function createWithdrawInternal($currency, $params, $currencyCt)
    {
        $receiverCurrencyUser = $params['receiverCurrencyUser'];
        $senderCurrencyUser = $this->currencyUserService->getCurrencyUser($currency->id, $params['member_id']);
        $hash = Str::random(32);
        // TODO: 추후 내부 출금일 경우 수수료 처리 삭제해야함.
        $amount = bcadd($params['amount'],$currencyCt['withdraw_fee'],$currency->decimal_point);
        DB::beginTransaction();
        // 출금 등록 (fee 없음)
        $coinTransactionWithdraw = $this->coinTransactionRepository->create([
            'member_id' => $params['member_id'],
            'type' => CoinTransactionRepository::TYPE_WITHDRAW_INTERNAL,
            'currency_id' => $currency->id,
            'hash' => $hash,
            'receiver' => $params['receiver'],
            'amount' => $amount,
            'status' => CoinTransactionRepository::STATUS_SUCCESS,
        ]);
        // 회원 자산 검증
        // 사용중인 assets
        $availableAssets = $this->userAssetsService->getAvailableUserAssetLock($params['member_id'], $currency->id, $currency->decimal_point);
        if (bccomp($availableAssets, $amount) <= 0) {
            throw new ApplicationException(ApplicationException::LOW_BALANCE);
        }
        // 보내는 회원 자산 차감 & 자산 로그
        $this->userAssetsService->subCurrencyUserAsset(UserAssetsLogRepository::TYPE_WITHDRAW, $currency->id, $params['member_id'], $amount, $coinTransactionWithdraw->id);

        // 입금 등록
        $coinTransactionDeposit = $this->coinTransactionRepository->create([
            'member_id' => $receiverCurrencyUser->member_id,
            'type' => CoinTransactionRepository::TYPE_DEPOSIT_INTERNAL,
            'currency_id' => $currency->id,
            'hash' => $hash,
            'sender' => $senderCurrencyUser->chongzhi_url,
            'receiver' => $params['receiver'],
            'amount' => $amount,
            'status' => CoinTransactionRepository::STATUS_SUCCESS,
        ]);
        // 받는 회원 자산 증감 & 자산 로그
        $this->userAssetsService->addCurrencyUserAsset(UserAssetsLogRepository::TYPE_DEPOSIT, $currency->id, $receiverCurrencyUser->member_id, $amount, $coinTransactionDeposit->id);
        DB::commit();
    }

    public function isSubAddressType($currencyId)
    {
        return $this->currencyInfoRepository->where(['id'=>$currencyId,'address_type'=>CurrencyInfoRepository::ADDRESS_TYPE_SUB_ADDRESS])->get()->isNotEmpty();
    }

    public function getTransactionList($params)
    {
        $query = $this->coinTransactionRepository->whereColumn('created_at', '>=', Carbon::createFromFormat('Y-m-d', $params['start'])->startOfDay())
            ->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $params['end'])->endOfDay())->where('member_id',$params['member_id']);
        if ($params['type'] != 'all') {
            $query = $query->where('type', $params['type'])->orWhere('type', $params['type'] . '_internal');
        }
        return $query->skip($params['offset'])->take($params['limit'])->get();
    }

    public function getTransactionListCount($params)
    {
        $query = $this->coinTransactionRepository->whereColumn('created_at', '>=', Carbon::createFromFormat('Y-m-d', $params['start'])->startOfDay())
            ->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $params['end'])->endOfDay())->where('member_id',$params['member_id']);
        if ($params['type'] != 'all') {
            $query = $query->where('type', $params['type']);
        }
        return $query->skip($params['offset'])->take($params['limit'])->count();
    }
}