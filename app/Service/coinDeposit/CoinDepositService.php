<?php


namespace App\Service\coinDeposit;


use App\Exceptions\ApplicationException;
use App\Repositories\CoinMergeRepository;
use App\Repositories\CoinTransactionRepository;
use App\Repositories\CurrencyCtRepository;
use App\Repositories\CurrencyInfoRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\DaemonTxRepository;
use App\Repositories\MemberRepository;
use App\Repositories\UsedAssetsRepository;
use App\Repositories\UserAssetsLogRepository;
use App\Service\coinDaemonLog\CoinDaemonLogAbstract;
use App\Service\common\UserAssetsService;
use App\Service\CurrencyService;
use App\Service\CurrencyUserService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CoinDepositService
{
    private $tagTypeCurrencyArr = [];

    const STATUS_DEPOSIT_ING = 'I';
    const STATUS_DEPOSIT_WAIT = 'W';
    const STATUS_DEPOSIT_SUCCESS = 'S';
    const STATUS_DEPOSIT_FAIL = 'F';

    const TYPE_COIN_TRANSACTION_DEPOSIT = 'deposit';
    const TYPE_USER_ASSETS_LOG_DEPOSIT = 'deposit';

    const DEPOSIT_STATUS_OPEN = 1;
    const DEPOSIT_STATUS_CLOSE = 1;

    /**
     * @var DaemonTxRepository
     */
    private $daemonTxRepository;
    /**
     * @var CurrencyService
     */
    private $currencyService;
    /**
     * @var CurrencyCtRepository
     */
    private $ctRepository;
    /**
     * @var CurrencyUserService
     */
    private $currencyUserService;
    /**
     * @var CoinTransactionRepository
     */
    private $coinTransactionRepository;
    /**
     * @var MemberRepository
     */
    private $memberRepository;
    /**
     * @var UserAssetsLogRepository
     */
    private $userAssetsLogRepository;
    /**
     * @var UserAssetsService
     */
    private $userAssetsService;
    /**
     * @var CoinMergeRepository
     */
    private $coinMergeRepository;

    /**
     * CoinDepositService constructor.
     * @param DaemonTxRepository $daemonTxRepository
     * @param CurrencyService $currencyService
     * @param CurrencyCtRepository $ctRepository
     * @param CurrencyUserService $currencyUserService
     * @param CoinTransactionRepository $coinTransactionRepository
     * @param MemberRepository $memberRepository
     * @param UserAssetsLogRepository $userAssetsLogRepository
     * @param UserAssetsService $userAssetsService
     * @param CoinMergeRepository $coinMergeRepository
     */
    public function __construct(DaemonTxRepository $daemonTxRepository, CurrencyService $currencyService
        , CurrencyCtRepository $ctRepository, CurrencyUserService $currencyUserService, CoinTransactionRepository $coinTransactionRepository
        , MemberRepository $memberRepository, UserAssetsLogRepository $userAssetsLogRepository, UserAssetsService $userAssetsService
        , CoinMergeRepository $coinMergeRepository)
    {
        $this->daemonTxRepository = $daemonTxRepository;
        $this->currencyService = $currencyService;
        $this->ctRepository = $ctRepository;
        $this->currencyUserService = $currencyUserService;
        $this->coinTransactionRepository = $coinTransactionRepository;
        $this->memberRepository = $memberRepository;
        $this->userAssetsLogRepository = $userAssetsLogRepository;
        $this->userAssetsService = $userAssetsService;
        $this->tagTypeCurrencyArr = $this->currencyService->getSubAddressTypeCurrencyIdArr();
        $this->coinMergeRepository = $coinMergeRepository;
    }

    /**
     * @param $coinSymbol
     * @throws ApplicationException
     * @throws Exception
     */
    public function process($coinSymbol)
    {
        $currencyId = $this->currencyService->getCurrencyIdBySymbol($coinSymbol);
        // get wait deposit list
        $daemonLogDepositList = $this->getWaitDaemonTxDeposit($currencyId);
        try {
            foreach ($daemonLogDepositList as $daemonLogDeposit) {
                // 통화 설정 데이터 추출
                $currency = $this->currencyService->getCurrency($daemonLogDeposit->currency_id);
                if ($currency->deposit_status == CurrencyRepository::STATUS_DEPOSIT_INACTIVE) {
                    $this->daemonTxRepository->where(['id' => $daemonLogDeposit->id, 'status_deposit' => DaemonTxRepository::STATUS_DEPOSIT_ING])->update(['status_deposit' => DaemonTxRepository::STATUS_WAIT]);
                    Log::alert("입금 금지 코인: ".json_encode($daemonLogDeposit->toArray()));
                    continue;
                }
                // TODO: krw 가격 조회 => 신규 로직으로 변경해야함. interface 문의
                // currency_user 테이블에서 회원 주소가 유효한지 체크
                $address = $daemonLogDeposit->receiver;
                $tag = $daemonLogDeposit->receiver_sub;
                $currencyUser = $this->currencyUserService->getUserAddressByCurrencyAddress($daemonLogDeposit->currency_id, $address, $tag);
                if (is_null($currencyUser)) {
                    $daemonLogDeposit->status_deposit = DaemonTxRepository::STATUS_FAIL;
                    $daemonLogDeposit->save();
                    Log::alert("회원주소가 아님: ".json_encode($daemonLogDeposit->toArray()));
                    continue;
                }
                // 같은 입금 정보가 입금 테이블에 있는지 체크
                if ($this->existsDeposit($daemonLogDeposit)) {
                    $daemonLogDeposit->status_deposit = DaemonTxRepository::STATUS_SUCCESS;
                    $daemonLogDeposit->save();
                    Log::alert("이미 존재하는 입금 데이터: ".json_encode($daemonLogDeposit->toArray()));
                    continue;
                }

                if ($daemonLogDeposit->status===DaemonTxRepository::STATUS_SUCCESS) {
                    DB::beginTransaction();
                    // 입금 테이블 저장
                    $coinTransaction = $this->createDepositByDaemonTx($daemonLogDeposit, $currencyUser->member_id);
                    // Log::debug("입금 테이블 저장");

                    // 회원 자산 처리
                    $this->userAssetsService->addCurrencyUserAsset(self::TYPE_USER_ASSETS_LOG_DEPOSIT, $daemonLogDeposit->currency_id, $currencyUser->member_id, $daemonLogDeposit->amount, $coinTransaction->id);
                    // Log::debug("회원 자산 처리");

                    // 데몬로그 성공 처리
                    $daemonLogDeposit->status_deposit = DaemonTxRepository::STATUS_SUCCESS;
                    $daemonLogDeposit->save();
                    // Log::debug("데몬로그 성공 처리");
                    DB::commit();
                    if ($currency->merge_active == CurrencyInfoRepository::MERGE_ACTIVE) {
                        $this->createCoinMerge($coinTransaction);
                    }
                } else {
                    DB::beginTransaction();
                    // 데몬로그 진행중 처리
                    $daemonLogDeposit->status_deposit = DaemonTxRepository::STATUS_ING;
                    $daemonLogDeposit->save();
                    // 입금 정보 insert
                    $this->createDepositByDaemonTx($daemonLogDeposit, $currencyUser->member_id);
                    DB::commit();
                }
            }
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $coinSymbol
     * @throws ApplicationException
     */
    public function checkProcess($coinSymbol)
    {
        $currencyId = $this->currencyService->getCurrencyIdBySymbol($coinSymbol);
        // get wait deposit list
        $coinTransactionDepositList = $this->getWaitDeposit($currencyId);
        try {
            foreach ($coinTransactionDepositList as $coinTransactionDeposit) {

                // TODO 통화 설정 데이터 추출 => 추후 캐쉬 적용 가능
                $currencyInfo = $this->currencyService->getCurrency($coinTransactionDeposit->currency_id);
                if ($currencyInfo->deposit_status != self::DEPOSIT_STATUS_OPEN) {
                    continue;
                }
                // daemonTx 상태 확인
                $daemonTx = $this->daemonTxRepository->find($coinTransactionDeposit->daemon_tx_id);

                if ($daemonTx->status==DaemonTxRepository::STATUS_SUCCESS) {
                    DB::beginTransaction();
                    // 회원 자산 처리
                    $this->userAssetsService->addCurrencyUserAsset(UsedAssetsRepository::TYPE_DEPOSIT, $coinTransactionDeposit->currency_id, $coinTransactionDeposit->member_id, $coinTransactionDeposit->amount, $coinTransactionDeposit->id);
                    // Log::debug("회원 자산 처리");

                    // 데몬로그 성공 처리
                    $daemonTx->status_deposit = DaemonTxRepository::STATUS_SUCCESS;
                    $daemonTx->save();
                    // 입금 데이터 성공 처리
                    $coinTransactionDeposit->status = CoinTransactionRepository::STATUS_SUCCESS;
                    $coinTransactionDeposit->save();
                    Log::debug("데몬로그 성공 처리");
                    DB::commit();

                    if ($currencyInfo->merge_active == CurrencyInfoRepository::MERGE_ACTIVE) {
                        $this->createCoinMerge($coinTransactionDeposit);
                    }
                } else if($daemonTx->status==DaemonTxRepository::STATUS_FAIL) {
                    Log::debug('fail');
                    $coinTransactionDeposit->status = CoinTransactionRepository::STATUS_FAIL;
                    $coinTransactionDeposit->save();
                } else {
                    Log::debug('wait');
                    $this->coinTransactionRepository->update($coinTransactionDeposit->id, ['status'=>CoinTransactionRepository::STATUS_WAIT]);
                    $coinTransactionDeposit->save();
                }
            }
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $currencyId
     * @return mixed
     * @throws Exception
     */
    private function getWaitDaemonTxDeposit($currencyId)
    {
        try {
            $where = ['type' => DaemonTxRepository::TYPE_DEPOSIT, 'status_deposit' => DaemonTxRepository::STATUS_DEPOSIT_WAIT];
            if (!is_null($currencyId)) {
                $where['currency_id'] = $currencyId;
            }
            $daemonTxs = $this->daemonTxRepository->where($where)->get();
            $daemonTxRepository = $this->daemonTxRepository;
            $updateDaemonTxs = $daemonTxs->filter(function ($daemonTx) use ($daemonTxRepository) {
                return $daemonTxRepository->where(['id' => $daemonTx->id, 'status_deposit' => DaemonTxRepository::STATUS_DEPOSIT_WAIT])->update(['status_deposit' => DaemonTxRepository::STATUS_DEPOSIT_ING]);
            });
            return $updateDaemonTxs;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $currencyId
     * @return mixed
     * @throws Exception
     */
    private function getWaitDeposit($currencyId)
    {
        $where = ['type' => self::TYPE_COIN_TRANSACTION_DEPOSIT, 'status' => self::STATUS_DEPOSIT_WAIT];
        if (!is_null($currencyId)) {
            $where['currency_id'] = $currencyId;
        }
        $depositWaitArr = $this->coinTransactionRepository->where($where)->get();
        $coinTransactionRepository = $this->coinTransactionRepository;
        $updateDeposit = $depositWaitArr->filter(function ($deposit) use ($coinTransactionRepository) {
            return $coinTransactionRepository->where(['id' => $deposit->id, 'status' => DaemonTxRepository::STATUS_DEPOSIT_WAIT])->update(['status' => DaemonTxRepository::STATUS_DEPOSIT_ING]);
        });
        return $updateDeposit;
    }

    public function existsDeposit($daemonLogDeposit)
    {
        return $this->coinTransactionRepository
            ->where(['type'=>self::TYPE_COIN_TRANSACTION_DEPOSIT,'currency_id'=>$daemonLogDeposit->currency_id, 'hash'=>$daemonLogDeposit->hash])
            ->get()
            ->isNotEmpty();
    }

    private function getMemberIdByAddress($daemonLogDeposit)
    {
        if (in_array($daemonLogDeposit->currency_id, $this->tagTypeCurrencyArr)) {
            if (empty($daemonLogDeposit->receiver_sub)) {
                Log::alert("태그형태 주소 정보가 없음: ".json_encode($daemonLogDeposit->toArray()));
                return null;
            }

            $member = $this->memberRepository->where(['show_uid'=>$daemonLogDeposit->receiver_sub])->first();
            if (is_null($member)) {
                Log::alert("입금된 태그 주소가 존재하지 않음: ".json_encode($daemonLogDeposit->toArray()));
                return null;
            }
            $memberId = $member->member_id;
        } else {
            $currencyUser = $this->currencyUserService->findByAddr($daemonLogDeposit->currency_id,$daemonLogDeposit->receiver);
            if (is_null($currencyUser)) {
                Log::alert("입금된 주소가 존재하지 않음: ".json_encode($daemonLogDeposit->toArray()));
                return null;
            }
            $memberId = $currencyUser->member_id;
        }
        return $memberId;
    }

    private function createDepositByDaemonTx($daemonLogDeposit, $memberId)
    {
        $insertParams = [
            'member_id'=>$memberId,
            'type'=>self::TYPE_COIN_TRANSACTION_DEPOSIT,
            'currency_id'=>$daemonLogDeposit->currency_id,
            'hash'=>$daemonLogDeposit->hash,
            'sender'=>$daemonLogDeposit->sender,
            'receiver'=>$daemonLogDeposit->receiver,
            'amount'=>$daemonLogDeposit->amount,
            'status'=>($daemonLogDeposit->status===DaemonTxRepository::STATUS_SUCCESS) ? CoinTransactionRepository::STATUS_SUCCESS : CoinTransactionRepository::STATUS_WAIT,
            'confirm'=>$daemonLogDeposit->confirm,
            'blocknumber'=>$daemonLogDeposit->blocknumber,
            'daemon_tx_id'=>$daemonLogDeposit->id,
        ];
        if (in_array($daemonLogDeposit->currency_id, $this->tagTypeCurrencyArr)) {
            $insertParams['receiver_sub'] = $daemonLogDeposit->receiver_sub;
        }
        return $this->coinTransactionRepository->create($insertParams);
    }

    /**
     * @param $coinTransaction
     */
    private function createCoinMerge($coinTransaction): void
    {
        $coinMergeParams = [
            'currency_info_id' => $coinTransaction->currency_id,
            'address' => $coinTransaction->receiver,
            'amount' => $coinTransaction->amount,
            'coin_transaction_id' => $coinTransaction->id,
        ];
        $this->coinMergeRepository->create($coinMergeParams);
    }
}