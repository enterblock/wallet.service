<?php

namespace App\Console;

use App\Console\Commands\CoinDaemonLogCheckProcess;
use App\Console\Commands\CoinDaemonLogProcess;
use App\Console\Commands\CoinMergeCheckProcess;
use App\Console\Commands\CoinMergeProcess;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\App;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command(CoinDaemonLogProcess::class, ['ETH'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogProcess::class, ['ETHToken'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogProcess::class, ['BTC'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogProcess::class, ['USDT'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['ETH'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['ETHToken'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['BTC'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['USDT'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogProcess::class, ['XRP'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogProcess::class, ['BCH'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogProcess::class, ['BSV'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogProcess::class, ['LTC'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['XRP'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['BCH'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['BSV'])->everyMinute()->runInBackground();
        $schedule->command(CoinDaemonLogCheckProcess::class, ['LTC'])->everyMinute()->runInBackground();

        $schedule->command(CoinMergeProcess::class)->everyMinute()->runInBackground();
        $schedule->command(CoinMergeCheckProcess::class)->everyMinute()->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'Asia/Seoul';
    }
}
