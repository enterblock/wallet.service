<?php

namespace App\Console\Commands;

use App\Exceptions\ApplicationException;
use App\Exceptions\DaemonException;
use App\Jobs\DaemonLogReceiptJob;
use App\Repositories\DaemonTxRepository;
use App\Service\coinDaemonLog\CoinDaemonLogFactory;
use App\Service\CurrencyService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CoinDaemonLogCheckProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinDaemonLog:check {symbol}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '코인 트랜잭션 DB화';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinDaemonLogFactory $coinDaemonLogFactory
     * @param CurrencyService $currencyService
     */
    public function handle(CoinDaemonLogFactory $coinDaemonLogFactory, CurrencyService $currencyService)
    {
        $symbol = $this->argument('symbol');
        try {
            $coinDaemonLogService = $coinDaemonLogFactory->build($symbol);
        } catch (Exception $e) {
            throw new $e;
        }

        // block 정보 추출
        $daemonTxs = $coinDaemonLogService->getWaitDaemonTx();
        if ($daemonTxs->count() > 0) {
            foreach ($daemonTxs as $daemonTx) {
                try {
                    $currency = $currencyService->getCurrency($daemonTx->currency_id);
                    $coinDaemonLogService->checkProcess($currency, $daemonTx);
                } catch (ApplicationException $e) {
                    if ($e->getCode() === ApplicationException::ABNORMAL_DATA) {
                        Log::error($e->getMessage());
                        $daemonTx->status = DaemonTxRepository::STATUS_FAIL;
                        $daemonTx->save();
                        continue;
                    }
                    $daemonTx->status = DaemonTxRepository::STATUS_WAIT;
                    $daemonTx->save();
                } catch (DaemonException $e) {
                    Log::error($e->getMessage());
                    $daemonTx->status = DaemonTxRepository::STATUS_WAIT;
                    $daemonTx->save();
                    continue;
                }
            }
        }
    }
}
