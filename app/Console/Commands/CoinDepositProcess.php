<?php

namespace App\Console\Commands;

use App\Exceptions\ApplicationException;
use App\Service\coinDeposit\CoinDepositService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CoinDepositProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinDeposit:process {--coin=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '코인 입금 로직';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinDepositService $coinDepositService
     */
    public function handle(CoinDepositService $coinDepositService)
    {
        $coin = $this->option('coin');
        try {
            $coinDepositService->process($coin);
        } catch (Exception $e) {
            Log::error($e);
        }
    }
}
