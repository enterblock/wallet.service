<?php

namespace App\Console\Commands;

use App\Exceptions\ApplicationException;
use App\Service\coinGenerateAddress\GenerateAddressFactory;
use App\Service\CoinService;
use App\Service\CurrencyService;
use App\Service\GenerateAddressService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GenerateAddressCheckProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coin:genAddressCheck {--coin=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '코인 주소 생성 확인';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CurrencyService $currencyService
     * @param CoinService $coinService
     * @param GenerateAddressFactory $generateAddressFactory
     * @throws ApplicationException
     */
    public function handle(CurrencyService $currencyService, CoinService $coinService, GenerateAddressFactory $generateAddressFactory)
    {
        $coin = $this->option('coin');

        if (!is_null($coin)) {
            $currency = $currencyService->findBySymbol($coin);
            if (is_null($currency)) {
                throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
            }
            $symbol = $currency->symbol;
            $generateAddrService = $generateAddressFactory->build($symbol);
            if (is_null($currency)) {
                throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
            }
            Log::info("GenAddressCheck Start : $symbol");
            $generateAddrService->checkProcess($currency);
        } else {
            $generateAddrCurrencyCtArr = $currencyService->getGenerateAddressCurrencyCt();
            foreach ($generateAddrCurrencyCtArr as $generateAddrCurrencyCt) {
                $currency = $generateAddrCurrencyCt->currency;
                $symbol = $currency->symbol;
                $generateAddrService = $generateAddressFactory->build($symbol);
                if (is_null($currency)) {
                    throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
                }
                Log::info("GenAddressCheck Start : $symbol");
                $generateAddrService->checkProcess($currency);
            }
        }
    }
}
