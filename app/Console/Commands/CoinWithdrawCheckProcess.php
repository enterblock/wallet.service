<?php

namespace App\Console\Commands;

use App\Service\coinWithdraw\CoinWithdrawService;
use Illuminate\Console\Command;

class CoinWithdrawCheckProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinWithdraw:check {--coin=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '출금 진행';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinWithdrawService $coinWithdrawService
     * @throws \App\Exceptions\ApplicationException
     * @throws \App\Exceptions\DaemonException
     */
    public function handle(CoinWithdrawService $coinWithdrawService)
    {
        $coin = $this->option('coin');
        $coinWithdrawService->checkProcess($coin);
    }
}
