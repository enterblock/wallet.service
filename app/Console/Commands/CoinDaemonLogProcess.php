<?php

namespace App\Console\Commands;

use App\Service\coinDaemonLog\CoinDaemonLogAbstract;
use App\Service\coinDaemonLog\CoinDaemonLogFactory;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CoinDaemonLogProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinDaemonLog:process {symbol} {--type=} {--block=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '코인 트랜잭션 DB화';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinDaemonLogFactory $coinDaemonLogFactory
     * @throws Exception
     */
    public function handle(CoinDaemonLogFactory $coinDaemonLogFactory)
    {
        $type = $this->option('type');
        $block = $this->option('block');
        $symbol = $this->argument('symbol');
        $type = $type?:'master';
        try {
            $coinDaemonLogService = $coinDaemonLogFactory->build($symbol);
        } catch (Exception $e) {
            dd($e);
        }

        if (is_null($block)) {
            // block 정보 추출
            /** @var CoinDaemonLogAbstract $coinDaemonLogService */
            $coinDaemonLogService->process($type, $symbol);
        } else {
            if (is_numeric($block)) {
                $coinDaemonLogService->processOnce($symbol, $block);
            } else {
                throw new Exception('Blocks can only accept numbers.');
            }
        }
    }
}
