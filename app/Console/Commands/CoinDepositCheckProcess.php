<?php

namespace App\Console\Commands;

use App\Service\coinDeposit\CoinDepositService;
use Illuminate\Console\Command;

class CoinDepositCheckProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinDeposit:check {--coin=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '코인 입금 확인 로직';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinDepositService $coinDepositService
     * @throws \App\Exceptions\ApplicationException
     */
    public function handle(CoinDepositService $coinDepositService)
    {
        $coin = $this->option('coin');
        try {
            $coinDepositService->checkProcess($coin);
        } catch (\Exception $e) {
            $this->line($e->getMessage());
        }
    }
}
