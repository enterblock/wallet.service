<?php

namespace App\Console\Commands;

use App\Service\coinclient\CoinClientFactory;
use App\Service\CoinService;
use App\Utils\StringUtils;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CoinTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coin:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinClientFactory $coinClientFactory
     * @throws \App\Exceptions\ApplicationException
     */
    public function handle(CoinClientFactory $coinClientFactory)
    {
        /*         xrp              */
        $client = $coinClientFactory->getClient('XRP');
//        $response = $client->getTransactionReceipt('C54B95C1CFA273A43813A297DCF9184FE681F04D0EFD4DCDF62F9CF93521223E');
//        dd($response);
        $resp = $client->sign([],[]);
        dd($resp);
    }
}
