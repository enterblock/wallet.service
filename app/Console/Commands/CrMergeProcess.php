<?php

namespace App\Console\Commands;

use App\Service\CoinService;
use App\Utils\StringUtils;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CrMergeProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coin:crMerge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CoinService $coinService)
    {
        /*         xrp              */
//        $client = $coinService->getClient('XRP');
//        $response = $client->getBlockHeight();

//        $response = $client->getTransactionList();
//        $transactions = collect($response["transactions"]);
//        $validReceiveTransaction = $transactions->filter(function ($tx) {
//            return $tx["validated"] && $tx["meta"]["TransactionResult"]==="tesSUCCESS" && ($tx["tx"]["Destination"]===env('XRP_ADDRESS'));
//        });
//        $validSendTransaction = $transactions->filter(function ($tx) {
//            return $tx["validated"] && $tx["meta"]["TransactionResult"]==="tesSUCCESS" && ($tx["tx"]["Account"]===env('XRP_ADDRESS'));
//        });
//        $totalReceiveAmount = $validReceiveTransaction->sum(function ($tx) {
//            return $tx["tx"]["Amount"];
//        });
//        $totalSendAmount = $validSendTransaction->sum(function ($tx) {
//            return $tx["tx"]["Amount"];
//        });
//        dd($validReceiveTransaction, $validSendTransaction, $totalReceiveAmount, $totalSendAmount, ($totalReceiveAmount - $totalSendAmount));

//        $txInfo = $client->getTransactionReceipt('320AB7DCED86FCDCE6ABD389813269447586AE1D31E2F541B8D26A49C7202F6C');
//        $txInfo = $client->getBlockHeight();
//        dd($txInfo);


        //dd(bcsub('34477970.3250045','90556.4237545',18));

        $ethCurrency = \App\Models\CurrencyInfo::find(187);
        $crCurrency = \App\Models\CurrencyInfo::find(193);

        $tibi = \App\Models\Tibi::select('url')->whereIn('status',[2,3,5])->where('currency_id',193)->groupBy('url')->get();
        $tibi->each(function ($deposit) use ($coinService, $crCurrency, $ethCurrency) {
            $balanceResult = $coinService->balance($crCurrency, $deposit->url);
            $crAmount = bcdiv(StringUtils::bcHexdec($balanceResult), bcpow('10',$crCurrency->decimal_point), $crCurrency->decimal_point);
            // 0 < amount
            if (bccomp('0', $crAmount) < 0) {
                Log::debug($deposit->url."|".$crAmount);
                $addr = $deposit->url;
                $balanceResult = $coinService->balance($ethCurrency, $addr);
                $amount = bcdiv(StringUtils::bcHexdec($balanceResult), bcpow('10',$ethCurrency->decimal_point), $ethCurrency->decimal_point);

                if (bccomp('0', $amount, $ethCurrency->decimal_point) < 0) {
                    /** CR이동 */
                    $params = [
                        'amount'=>$crAmount,
                        'sender'=>$addr
                    ];
                    $withdrawResult = $coinService->merge($crCurrency, $params);
                    Log::debug('CR|'.$addr.'|'.$withdrawResult.'|'.$crAmount);
                    /** CR이동 */
                } else {
                    /** 수수료 전송 */
                    $params = [
                        'amount'=>'0.0005',
                        'receiver'=>$addr
                    ];
                    $withdrawResult = $coinService->withdraw($ethCurrency, $params);
                    Log::debug('ETH|'.$addr.'|'.$withdrawResult.'|0.0005');
                    /** 수수료 전송 */
                }
            }
        });
    }
}
