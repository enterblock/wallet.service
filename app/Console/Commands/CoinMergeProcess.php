<?php

namespace App\Console\Commands;

use App\Service\CoinMergeService;
use Illuminate\Console\Command;

class CoinMergeProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinMerge:process {--coin=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '코인 이동처리';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param CoinMergeService $coinMergeService
     * @return mixed
     */
    public function handle(CoinMergeService $coinMergeService)
    {
        $coin = $this->option('coin');
        $coinMergeService->process($coin);
    }
}
