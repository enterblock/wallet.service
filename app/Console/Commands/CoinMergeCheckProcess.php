<?php

namespace App\Console\Commands;

use App\Service\CoinMergeService;
use Illuminate\Console\Command;

class CoinMergeCheckProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coinMerge:check {--coin=} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '코인 이동처리 검증';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CoinMergeService $coinMergeService
     * @throws \App\Exceptions\ApplicationException
     */
    public function handle(CoinMergeService $coinMergeService)
    {
        $coin = $this->option('coin');
        $coinMergeService->checkProcess($coin);
    }
}
