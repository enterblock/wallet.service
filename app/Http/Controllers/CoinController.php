<?php

namespace App\Http\Controllers;

use App\Exceptions\ApplicationException;
use App\Http\Resources\ApiResource;
use App\Http\Resources\FailCollection;
use App\Models\Currency;
use App\Models\DaemonTx;
use App\Models\GenerateAddress;
use App\Service\coinDaemonLog\CoinDaemonLogFactory;
use App\Service\CoinService;
use App\Service\common\UserAssetsService;
use App\Service\CurrencyService;
use App\Service\MemberService;
use App\Utils\StringUtils;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CoinController extends Controller
{
    /**
     * @var CoinService
     */
    private $coinService;
    /**
     * @var CoinDaemonLogFactory
     */
    private $coinDaemonLogFactory;
    /**
     * @var CoinDaemonLogFactory
     */
    private $daemonLogFactory;
    /**
     * @var CurrencyService
     */
    private $currencyService;
    /**
     * @var MemberService
     */
    private $memberService;
    /**
     * @var UserAssetsService
     */
    private $userAssetsService;

    public function __construct(CoinService $coinService,CoinDaemonLogFactory $coinDaemonLogFactory
        ,CoinDaemonLogFactory $daemonLogFactory, CurrencyService $currencyService, MemberService $memberService, UserAssetsService $userAssetsService)
    {
        $this->coinService = $coinService;
        $this->coinDaemonLogFactory = $coinDaemonLogFactory;
        $this->daemonLogFactory = $daemonLogFactory;
        $this->currencyService = $currencyService;
        $this->memberService = $memberService;
        $this->userAssetsService = $userAssetsService;
    }

    /**
     * @param Request $request
     * @param int $currencyId
     * @return ApiResource
     * @throws ApplicationException
     */
    public function getNewAddress(Request $request, int $currencyId)
    {
        $currency = $this->currencyService->getCurrency($currencyId);
        if (is_null($currency)) {
            throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
        }

        $data = $request->input();

        $validator = Validator::make($data, [
            'member_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            throw new ApplicationException(ApplicationException::REQUEST_PARAM, $errors); // 요청 파라메터 에러.
        }

        // 회원 검증
        $member = $this->memberService->getMember($data['member_id']);
        if (is_null($member)) {
            throw new ApplicationException(ApplicationException::NOT_MEMBER); // 회원정보가 존재하지 않음
        }

        return new ApiResource($this->coinService->createCoinAddr($currency, $member));
    }

    /**
     * @param Request $request
     * @param int $currencyId
     * @return ApiResource|FailCollection
     * @throws ApplicationException
     */
    public function withdraw(Request $request)
    {
        try {
            $params = $request->input();
            $validator = Validator::make($params, [
                'member_id' => 'required|numeric',
                'receiver' => 'required',
                'amount' => 'required',
                'currency_id' => 'required|numeric',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                return new FailCollection(collect($errors->all()));
            }

            $currency = $this->currencyService->getCurrency($params['currency_id']);
            if (is_null($currency)) {
                throw new ApplicationException(ApplicationException::UNSUPPORTED_COIN);
            }

            $this->coinService->createWithdraw($currency,$params);

            return new ApiResource([]);
        } catch (ApplicationException $e) {
            throw $e;
        }
    }

    public function getTransactionList(Request $request)
    {
        $params = $request->input();
        $validator = Validator::make($params, [
            'member_id' => 'required|numeric',
            'start' => 'required',
            'end' => 'required',
            'type' => ['regex:/^all$|^withdraw$|^deposit$/i'],
            'offset' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return new FailCollection(collect($errors->all()));
        }
        return new ApiResource($this->coinService->getTransactionList($params));
    }

    public function getTransactionListCount(Request $request)
    {
        $params = $request->input();
        $validator = Validator::make($params, [
            'member_id' => 'required|numeric',
            'start' => 'required',
            'end' => 'required',
            'type' => ['regex:/^all$|^withdraw$|^deposit$/i'],
            'offset' => 'required|numeric',
            'limit' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return new FailCollection(collect($errors->all()));
        }
        return new ApiResource($this->coinService->getTransactionListCount($params));
    }

    public function test()
    {
        /*         xrp              */
//        $client = $this->coinService->getClient('XRP');
//        $response = $client->getTransactionReceipt('C54B95C1CFA273A43813A297DCF9184FE681F04D0EFD4DCDF62F9CF93521223E');
//        dd($response);
//        $resp = $client->sign([],[]);
//        dd($resp);

//        $coinDaemonLogService = $this->coinDaemonLogFactory->build('XRP');
//        $coinDaemonLogService->process('master', 'XRP');
//        $daemonTxs = DaemonTx::whereNull('fee')->get();
//        foreach ($daemonTxs as $daemonTx) {
//            $coin = $this->currencyService->find($daemonTx->currency_id);
//            $daemonLogService = $this->daemonLogFactory->build($coin->currency_mark);
//            $daemonLogService->checkProcess($coin, $daemonTx);
//        }

//        $daemonTx = DaemonTx::find(3);
//        $coin = $this->currencyService->find($daemonTx->currency_id);
//        $daemonLogService = $this->daemonLogFactory->build($coin->currency_mark);
//        $daemonLogService->checkProcess($coin, $daemonTx);

//        $response = $client->getTransactionList();
//        $transactions = collect($response["transactions"]);

//        $txInfo = $client->getTransactionReceipt('320AB7DCED86FCDCE6ABD389813269447586AE1D31E2F541B8D26A49C7202F6C');
//        $txInfo = $client->getBlockHeight();
//        dd($txInfo);
        /*              cr           */
//        $currency = \App\Models\CurrencyInfo::find(187);
//        // 34795.3250045
//        $params = [
//            'amount'=>'0.01',
//            'receiver'=>'0x196eaacb0e47dbd0787b3f60de4f2195e60fbabf'
//        ];
//        dd($params);
//        $withdrawResult = $this->coinService->withdraw($currency, $params);
//        dd($withdrawResult);

//        $tibi = \App\Models\Tibi::find(26798);
//        dd($tibi->currency_id);
//        $balanceResult = $this->coinService->balance($currency, '0x196eaacb0e47dbd0787b3f60de4f2195e60fbabf');
//        dd(bcdiv(StringUtils::bcHexdec($balanceResult), bcpow('10',$currency->decimal_point), $currency->decimal_point));

        /*                    ETH               */
//        $coinDaemonLogService = $this->coinDaemonLogFactory->build('ETH');
//        $coinDaemonLogService->process('master', 'ETH');

//        $currency = \App\Models\CurrencyInfo::find(187);
//        // 34795.3250045
//        $params = [
//            'amount'=>'0.2',
//            'receiver'=>'0xf4612422212b6018f2d3132056f69dccc79ba2b1'
//        ];
//        $withdrawResult = $this->coinService->withdraw($currency, $params);
//        dd($withdrawResult);
//        $this->userAsstsService->addCurrencyUserAsset('test', 193, 132840, 1, 1);
//        $currency = \App\Models\Currency::find(214);
//        $client = $this->coinService->getClient($currency);
//        $setGasPrice = 4000000000;
//        $setGas = 56642;
//
//        $pow = bcpow('10', $currency->decimal_point);
//        $attributes['hexAmount'] = StringUtils::bcDechex(bcmul('1000', $pow));
//        $attributes['receiver'] = env('ETH_OUT_ADDRESS');
//        $sendData = $client->generateSendData($attributes);
//        $password = env('ETH_OUT_ADDRESS_PASSWORD');
//        $sendAttributes = [
//            [
//                "from"=>'0x2aad412791abefb0bf91ad8e8bb2960cc4fa8664',
//                "to"=>'0x6b40d317bc1de4b0938519ac707ae36464f49171',
//                "value"=>"0x0",
//                "data"=>$sendData,
//                "gasPrice" => '0x'.StringUtils::bcDechex($setGasPrice),
//                "gas" => '0x'.StringUtils::bcDechex($setGas),
//                "nonce" => '0x'.StringUtils::bcDechex(0),
//            ],
//            $password
//        ];
//        dd($sendAttributes);
//        dd($client->sendTrnsaction($sendAttributes));


        /*              btc           */
//        $currency = \App\Models\CurrencyInfo::find(185);
//        // 34795.3250045
//        $params = [
//            'amount'=>'0.02',
//            'receiver'=>'1J2UVwbrMLebk13seUnDBxFvDcusAJ2f5K'
//        ];
//        $withdrawResult = $this->coinService->withdraw($currency, $params);
//        dd($withdrawResult);
    }
}
