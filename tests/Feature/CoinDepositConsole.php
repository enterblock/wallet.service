<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CoinDepositConsole extends TestCase
{

    function test_success()
    {
        $this->artisan('coinDeposit:process')
            ->assertExitCode(0);
    }

    function test_coin_option_success()
    {
        $this->artisan('coinDeposit:process', ['--coin' => 'XRP'])
            ->assertExitCode(0);
    }

    function test_check_success()
    {
        $this->artisan('coinDeposit:check')
            ->assertExitCode(0);
    }

    function tests_fail_request_path()
    {
        // TODO: Implement tests_fail_request_path() method.
    }

    function test_fail_request_params()
    {
        $this->artisan('coinDeposit:process', ['--coin' => 111])
            ->expectsOutput("지원하지 않는 코인")
            ->assertExitCode(0);
    }
}
