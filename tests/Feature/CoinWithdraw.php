<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CoinWithdraw extends TestCase
{
    function test_success()
    {
        $response = $this->post('/api/coin/withdraw/187', [
            'member_id'=>134365,
            'receiver'=>'0xe49d8e04a37b2c5a9d5208abc4737500fe58c642',
            'amount'=>'5.555555550000000000',
        ]);

        $response->assertJson([
            'status' => 'success'
        ])
        ->assertJsonStructure([
            'status',
            'data'
        ])
        ->assertStatus(200);
    }

    function test_btc_success()
    {
        $response = $this->post('/api/coin/withdraw/185', [
            'member_id'=>134365,
            'receiver'=>'33swTcjxqSrkseKyhHt25WGR4tCjjfiHZZ',
            'amount'=>'0.0001',
        ]);

        $response->assertJson([
            'status' => 'success'
        ])
        ->assertJsonStructure([
            'status',
            'data'
        ])
        ->assertStatus(200);
    }

    function test_internal_withdraw_success()
    {
        $response = $this->post('/api/coin/withdraw/187', [
            'member_id'=>134365,
            'receiver'=>'0x5cc6ece39e90c4bbd61a6efbfdd8b2ef7ff25966',
            'amount'=>'6.6666666660000000000',
        ]);

        $response->assertJson([
            'status' => 'success'
        ])
            ->assertJsonStructure([
                'status',
                'data'
            ])
            ->assertStatus(200);
    }

    function test_internal_xrp_withdraw_success()
    {
        $response = $this->post('/api/coin/withdraw', [
            'currency_id'=>194,
            'member_id'=>135382,
            'receiver'=>'rGT84ryubURwFMmiJChRbWUg9iQY18VGuQ',
            'receiver_sub'=>'992893230',
            'amount'=>'1',
        ]);

        $response->assertJson([
            'status' => 'success'
        ])
            ->assertJsonStructure([
                'status',
                'data'
            ])
            ->assertStatus(200);
    }

    function tests_fail_request_path()
    {
        $response = $this->get('/api/coin/test');

        $response->assertJson([
            'status' => 'success'
        ])
        ->assertJsonStructure([
            'status',
            'data'
        ])
        ->assertStatus(200);
    }

    function test_fail_request_params()
    {
        // TODO: Implement test_fail_request_params() method.
    }
}
