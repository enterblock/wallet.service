<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CoinTransaction extends TestCase
{
    function test_success()
    {
        $response = $this->get('/api/coin/transaction_list?member_id=134368&start=2019-11-23&end=2019-11-30&type=all&offset=0&limit=10');

        $response->assertStatus(200);
    }

    function test_count_success()
    {
        $response = $this->get('/api/coin/transaction_list_count?member_id=134368&start=2019-11-23&end=2019-11-30&type=all&offset=0&limit=10');

        $response->assertStatus(200);
    }

    function tests_fail_request_path()
    {
        // TODO: Implement tests_fail_request_path() method.
    }

    function test_fail_request_params()
    {
        // TODO: Implement test_fail_request_params() method.
    }
}
