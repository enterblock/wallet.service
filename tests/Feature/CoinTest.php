<?php

namespace Tests\Feature;

use App\Models\CurrencyInfo;
use App\Models\GenerateAddressTransaction;
use App\Service\coinclient\BTCClient;
use App\Service\coinclient\ETHLegacyClient;
use App\Service\coinclient\USDTClient;
use App\Utils\StringUtils;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class CoinTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testWithdraw()
    {
        $response = $this->get('/api/coin/test');

        $response->assertStatus(200);
    }

    function test_json()
    {
        $json = preg_replace('/\: *([-]?[0-9]+\.?[0-9e+\-]*)/', ':"\\1"', '{"data": "-123.123124"}');
        echo $json;
    }

    function tests_send_legacy()
    {
        $client = new ETHLegacyClient();
        $currencyService = $this->app->make('App\Service\CurrencyService');
        $currency = $currencyService->findBySymbol('ETH');
        $balanceHex = $client->balance($currency, env('ETH_OUT_ADDRESS'));
        $balance = bcdiv(StringUtils::bcHexdec($balanceHex), bcpow('10', $currency->decimal_point), $currency->decimal_point);
        //dd($balance);
        //$balance = bcsub($balance, '0.0000945', $currency->decimal_point);
        $sendParams = [
            'receiver'=>'0xf4612422212b6018f2d3132056f69dccc79ba2b1',
            'amount'=>$balance,
        ];
        dd($sendParams);
        $txid = $client->send($currency, $sendParams);
    }

    function test_usdt()
    {
        $currencyService = $this->app->make('App\Service\CurrencyService');
        $currency = $currencyService->findBySymbol('USDT');
        $client = new USDTClient();
//        dd($client->getListBlock(100000));
//        dd($client->getBalance());
//        dd($client->merge($currency,[
//            'sender' => '1KP8TPcVpMoXpeUD6SrM8gbifGZ8mowhAJ',
//            'amount' => '7.39',
//        ]));
//        dd($client->getBlockHeight());
        dd($client->balance($currency,'1KP8TPcVpMoXpeUD6SrM8gbifGZ8mowhAJ'));
    }
    function test_btc()
    {
        $currencyService = $this->app->make('App\Service\CurrencyService');
        $currency = $currencyService->findBySymbol('BTC');
        $client = new BTCClient();
//        dd($client->getListBlock(100000));
//        dd($client->getBalance());
//        dd($client->merge($currency,[
//            'sender' => '1KP8TPcVpMoXpeUD6SrM8gbifGZ8mowhAJ',
//            'amount' => '7.39',
//        ]));
//        dd($client->getBlockHeight());
//        dd($client->getBalance());
        $sendParams = [
            '1J2UVwbrMLebk13seUnDBxFvDcusAJ2f5K',
            '0.001',
        ];
        dd($sendParams);
        $txid = $client->send($currency, $sendParams);
        dd($txid);
    }
}
