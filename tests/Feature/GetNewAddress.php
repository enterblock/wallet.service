<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetNewAddress extends TestCase
{
    /**
     * 주소생성 테스트 성공.
     *
     * @return void
     */
    public function test_success()
    {
        $response = $this->post('/api/coin/new_address/187', ['member_id'=>134365]);

        $response
            ->assertJson([
                'status' => 'success'
            ])
            ->assertJsonStructure([
                'status',
                'data'
            ])
            ->assertStatus(200);
    }

    /**
     * 주소생성 테스트 성공. xrp -> tag type
     *
     * @return void
     */
    public function test_success_xrp()
    {
        $response = $this->post('/api/coin/new_address/194', ['member_id'=>134365]);

        $response
            ->assertJson([
                'status' => 'success'
            ])
            ->assertJsonStructure([
                'status',
                'data'
            ])
            ->assertStatus(200);
    }

    /**
     * 주소생성 테스트 성공. erc20
     *
     * @return void
     */
    public function test_success_erc20()
    {
        $response = $this->post('/api/coin/new_address/214', ['member_id'=>135382]);

        $response
            ->assertJson([
                'status' => 'success'
            ])
            ->assertJsonStructure([
                'status',
                'data'
            ])
            ->assertStatus(200);
    }

    /**
     * 주소생성 테스트 실패. (코인 정보 존재하지 않음.)
     *
     * @return void
     */
    public function tests_fail_request_path()
    {
        $response = $this->post('/api/coin/new_address/18777777', ['member_id'=>134365]);

        $response
            ->assertJson([
                'status' => 'fail'
            ])
            ->assertStatus(500);
    }

    /**
     * 주소생성 테스트 실패. (회원 정보 존재하지 않음.)
     *
     * @return void
     */
    public function test_fail_request_params()
    {
        $response = $this->post('/api/coin/new_address/187', []);

        $response
            ->assertJson([
                'status' => 'fail'
            ])
            ->assertStatus(400);
    }

    /**
     * 주소생성 테스트 실패. (회원 정보 존재하지 않음.)
     *
     * @return void
     */
    public function test_is_not_member()
    {
        $response = $this->post('/api/coin/new_address/187', ['member_id'=>1111211111]);

        $response
            ->assertJson([
                'status' => 'fail'
            ])
            ->assertStatus(500);
    }
}
