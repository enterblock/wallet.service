<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GenerateAddressConsole extends TestCase
{

    function test_process_success()
    {
        $this->artisan('coin:genAddress')
            ->assertExitCode(0);
    }

    function test_process_option_success()
    {
        $this->artisan('coin:genAddress', ['--coin' => 'BTC'])
            ->assertExitCode(0);
    }

    function test_check_success()
    {
        $this->artisan('coin:genAddressCheck')
            ->assertExitCode(0);
    }

    function tests_fail_request_path()
    {
        //$result = $this->artisan('coin:genAddress', ['--coin1' => 111])->run();
    }

    function test_fail_request_params()
    {
        $this->artisan('coin:genAddress', ['--coin' => 111])
            ->expectsOutput("지원하지 않는 코인")
            ->assertExitCode(0);
    }
}
