<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CoinWithdrawConsole extends TestCase
{

    function test_success()
    {
        $this->artisan('coinWithdraw:process')
            ->assertExitCode(0);
    }

    function test_check_success()
    {
        $this->artisan('coinWithdraw:check')
            ->assertExitCode(0);
    }

    function tests_fail_request_path()
    {
        // TODO: Implement tests_fail_request_path() method.
    }

    function test_fail_request_params()
    {
        // TODO: Implement test_fail_request_params() method.
    }
}
