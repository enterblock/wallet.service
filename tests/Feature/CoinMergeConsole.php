<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CoinMergeConsole extends TestCase
{
    function test_all_success()
    {
        $this->artisan('coinMerge:process')
            ->assertExitCode(0);
    }

    function test_check_success()
    {
        $this->artisan('coinMerge:check')
            ->assertExitCode(0);
    }

    function tests_eth_success()
    {
        $this->artisan('coinMerge:process --coin=ETH')
            ->assertExitCode(0);
    }

    function tests_cr_success()
    {
        $this->artisan('coinMerge:process --coin=CR')
            ->assertExitCode(0);
    }

    function tests_btc_fail()
    {
        $this->artisan('coinMerge:process --coin=BTC')
            ->assertExitCode(0);
    }
}
