<?php

return [
    'out_address' => env('ETH_OUT_ADDRESS'),
    'out_address_password' => env('ETH_OUT_ADDRESS_PASSWORD'),
];