<?php

use App\Service\CoinService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('coin')->group(function () {
    Route::post('/new_address/{currencyId}', 'CoinController@getNewAddress');
    Route::post('/withdraw', 'CoinController@withdraw');
    Route::get('/transaction_list', 'CoinController@getTransactionList');
    Route::get('/transaction_list_count', 'CoinController@getTransactionListCount');
    Route::get('/test', 'CoinController@test');

});