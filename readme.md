## Network diagram

![flowchart](http://www.ebexchange.tk/img/wallet_service_infra.png)

## Blockchain deposit flowchart

![flowchart](http://www.ebexchange.tk/img/daemon_log_flowchart.png)

## Blockchain create address flowchart

![flowchart](http://www.ebexchange.tk/img/create_addr.png)

## git clone 방법
```
# submodule 포함
git clone --recursive https://enterblock@bitbucket.org/enterblock/wallet.service.git
```
Or
```
# clone 후 submodule update
git clone https://enterblock@bitbucket.org/enterblock/wallet.service.git
git submodule update --init --recursive
```

## composer install
```
composer install
```

## env 셋팅(DB 정보 & daemon server 정보)

```
APP_NAME=wallet_service
APP_ENV=production
APP_KEY=
APP_DEBUG=false
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
#DB_HOST=mysql.purcow
DB_HOST=127.0.0.1
#DB_PORT=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
DB_PREFIX="t_"

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=redis
SESSION_DRIVER=file
SESSION_LIFETIME=120

REDIS_HOST=
REDIS_PASSWORD=
REDIS_PORT=
REDIS_QUEUE=default
REDIS_CLIENT=phpredis
REDIS_DB=0
REDIS_CACHE_DB=0

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

######################################### ETH
ETH_NODE_IP=
ETH_NODE_PORT=
ETH_SIGN_PORT=
ETH_OUT_ADDRESS=
ETH_MANAGER_ADDRESS=
ETH_FEE_ADDRESS=
ETH_OUT_ADDRESS_PASSWORD=
ETH_DAEMON_ONCE_SEARCH_CNT=10
ETH_MAX_GET_USER_LENGTH=200 # 주소생성 로직에서 한번에 조회할 주소 개수
ETH_MAX_CREATE_USER_LENGTH=20 # 주소생성 트랜잭션 발생 시 한번에 생성할 개수

# 50 Gwai
GAS_PRICE_LIMIT=50000000000
GAS_PRICE_RATE=1.5
GAS_LIMIT=200000
GAS_RATE=1.5
######################################### ETH END

######################################### BCH
BCH_NODE_IP=
BCH_NODE_PORT=
BCH_NODE_USER=
BCH_NODE_PASSWORD=
BCH_OUT_PASSWORD=
BCH_UNLOCK_TIME=30
######################################### BCH END

######################################### BSV
BSV_NODE_IP=
BSV_NODE_PORT=
BSV_NODE_USER=
BSV_NODE_PASSWORD=
BSV_OUT_PASSWORD=
BSV_UNLOCK_TIME=30
######################################### BSV END

######################################### BTC
BTC_NODE_IP=
BTC_NODE_PORT=
BTC_NODE_USER=
BTC_NODE_PASSWORD=
BTC_OUT_PASSWORD=
BTC_UNLOCK_TIME=30
######################################### BTC END

######################################### LTC
LTC_NODE_IP=
LTC_NODE_PORT=
LTC_NODE_USER=
LTC_NODE_PASSWORD=
LTC_OUT_PASSWORD=
LTC_UNLOCK_TIME=30
######################################### LTC END

######################################### USDT
USDT_NODE_IP=
USDT_NODE_PORT=
USDT_SIGN_PORT=
USDT_OUT_ADDRESS=
USDT_MANAGER_ADDRESS=
USDT_FEE_ADDRESS=
USDT_OUT_ADDRESS_PASSWORD=
USDT_DAEMON_ONCE_SEARCH_CNT=50
USDT_MAX_GET_USER_LENGTH=200
USDT_MAX_CREATE_USER_LENGTH=20 # 주소생성 트랜잭션 발생 시 한번에 생성할 개수
######################################### USDT END

LOG_SLACK_WEBHOOK_URL=

LOG_SLACK_WEBHOOK_ERROR_URL=
LOG_SLACK_LEVEL=warning

XRP_BASE_REST_URL=https://data.ripple.com/v2
XRP_BASE_RPC_URL=https://s2.ripple.com:51234
XRP_WITHDRAW_RPC_IP=
XRP_WITHDRAW_RPC_PORT=
XRP_ADDRESS=
XRP_PASSWORD=

GENERATE_ADDRESS_COIN=ETH
GENERATE_ADDRESS_MIN_LIMIT=20 # 할당 대기중인 최소 개수

# DB 컬럼 decimal
DB_DECIMAL=18

```

## ERD

![flowchart](http://www.ebexchange.tk/img/erd.png)

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Seo, DongHyuk via [seodonghyuk@gmail.com](mailto:seodonghyuk@gmail.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
